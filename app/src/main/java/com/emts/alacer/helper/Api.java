package com.emts.alacer.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {

    private static Api api;

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }

    }

    public final String apiKey = "EMTSalacer";

    private String getIp() {
        return "http://nepaimpressions.com/dev/alacer/api/user/";
    }

    public final String registerUrl = getIp() + "signup";
    public final String getDepartmentUrl = getIp() + "department_list";
    public final String getBranchUrl = getIp() + "branch_list";
    public final String loginUrl = getIp() + "login";
    public final String forgetPasswordUrl = getIp() + "forgot_password";
    public final String inboxUrl = getIp() + "inbox";
    public final String changeReadStatusUrl = getIp() + "change_msg_status_to_read";
    public final String notificationResponseUrl = getIp() + "response";
    public final String needResponseUrl = getIp() + "need_response";
    public final String bcmPlan = getIp() + "bcm_plans";
    public final String reportingTypeUrl = getIp() + "reporting_type";
    public final String cmsUrl = getIp() + "cms";
    public final String incidentReportUrl = getIp() + "user_report";
    public final String deleteUrl = getIp() + "delete_msg";


    //super user
    public final String superUserLoginUrl = getIp() + "superuser_login";
    public final String incidentListSuperUrl = getIp() + "incident_list";
    public final String massNotificationUrl = getIp() + "super_user_mass_notification";
    public final String notificationStatusListing = getIp() + "notification_list_by_type";
    public final String massNotificationStatusDetail = getIp() + "notification_response";
    public final String resendMassNotification = getIp() + "resend_mass_notification";
    public final String getRespondedUser = getIp() + "get_responded_user";



    public final String upgradeDevicetoken = getIp() + "update_push";

}