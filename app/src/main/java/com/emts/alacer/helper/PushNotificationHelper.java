package com.emts.alacer.helper;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by User on 2017-09-18.
 */

public class PushNotificationHelper {
    private final static String AUTH_KEY_FCM = "AAAAQ-Q0Qh0:APA91bFa260bWslGlOdipp2VYi0O57yflTCbG4IFAkwHJSRRRnjnIpdo6zYYPkvu17WcXIS2hJfuohFYHyidBnvVsvLxyXJO7ttBO9JBRssYJ2LYBeX4LRMRSBYA3FONWiS9pRoU9hSc";
    private final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public static void sendNotification(String deviceId) {
        new SendPushAsync().execute(deviceId);
    }

    public static class SendPushAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Logger.e("PushNotificationHelper", "onPreExecute");
        }

        @Override
        protected String doInBackground(String... strings) {
            return sendPushNotification(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Logger.e("PushNotificationHelper", "onPostExecute");
            Logger.e("PushNotificationHelper result is:", s + " ");
        }
    }

    private static String sendPushNotification(String deviceToken) {
        Logger.e("PushNotificationHelper pushId", deviceToken + " ");
        String result = "";
        URL url = null;
        try {
            url = new URL(API_URL_FCM);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM);
            conn.setRequestProperty("Content-Type", "application/json");

            JSONObject json = new JSONObject();

            json.put("to", deviceToken.trim());
            JSONObject info = new JSONObject();
            info.put("title", "notification title"); // Notification title
            info.put("body", "message body"); // Notification
            // body
            json.put("notification", info);
            Logger.e("PushNotificationHelper post body", json.toString() + "");
            OutputStreamWriter wr = new OutputStreamWriter(
                    conn.getOutputStream());
            wr.write(json.toString());
            wr.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                result = result + output;
            }
        } catch (JSONException e) {
            Logger.e("PushNotification Helper e12", e.getMessage() + " ");
        } catch (ProtocolException e) {
            Logger.e("PushNotification Helper e143512", e.getMessage() + " ");
        } catch (MalformedURLException e) {
            Logger.e("PushNotification Helper e24553", e.getMessage() + " ");
        } catch (IOException e) {
            Logger.e("PushNotification Helper e56432", e.getMessage() + " ");
        }


        System.out.println("FCM Notification is sent successfully");

        return result;

    }
}
