package com.emts.alacer;

import android.app.Application;

/**
 * Created by User on 2017-04-07.
 */

public class AlacerApp extends Application {
    static AlacerApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

    }

    public static synchronized AlacerApp getInstance() {
        return mInstance;
    }
}
