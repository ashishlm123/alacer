package com.emts.alacer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.activity.superuser.RespondedListingActivity;
import com.emts.alacer.activity.superuser.RespondedUserDetail;
import com.emts.alacer.model.User;

import java.util.ArrayList;

/**
 * Created by User on 2017-08-03.
 */
public class RespondedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<User> rList;

    public RespondedAdapter(Context context, ArrayList<User> rList) {
        this.context = context;
        this.rList = rList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(
                R.layout.item_responded, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        User report = rList.get(position);

        viewHolder.tvContactNo.setText(report.getMobile());
        viewHolder.tvDepartName.setText(report.getDepartment());
        viewHolder.tvUserName.setText(report.getName());
    }

    @Override
    public int getItemCount() {
        return rList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvDepartName, tvContactNo;

        ViewHolder(View itemView) {
            super(itemView);

            tvUserName = itemView.findViewById(R.id.user_name);
            tvDepartName = itemView.findViewById(R.id.depart_name);
            tvContactNo = itemView.findViewById(R.id.contact_num);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, RespondedUserDetail.class);
                    intent.putExtra("user", rList.get(getLayoutPosition()));
                    intent.putExtra("isResponded", ((RespondedListingActivity)context).isResponded);
                    context.startActivity(intent);
                }
            });
        }
    }
}
