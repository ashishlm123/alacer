package com.emts.alacer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.activity.superuser.StatusDetailActivity;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.model.Status;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Srijana on 4/20/2017.
 */

public class NotificationStatusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Status> statusList;
    SimpleDateFormat sdf;
    //    Calendar calendar;
    SimpleDateFormat sdf1;
    SimpleDateFormat sdf2;

    public NotificationStatusAdapter(Context context, ArrayList<Status> statusList) {
        this.context = context;
        this.statusList = statusList;
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf1 = new SimpleDateFormat("dd MMM yyyy");
        sdf2 = new SimpleDateFormat("hh:mm aaa");
//        calendar = Calendar.getInstance();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_noti_status, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Status status = statusList.get(position);

        int totalSent;
        try {
            totalSent = Integer.parseInt(status.getTotalSend());
        } catch (Exception e) {
            totalSent = 0;
        }
        int totalRespond;
        try {
            totalRespond = Integer.parseInt(status.getTotalResponse());
        } catch (Exception e) {
            totalRespond = 0;
        }
        float respondPercentage = (totalRespond / (float) totalSent) * 100;
        viewHolder.respondStatus.setText((int) respondPercentage + "% ");
        viewHolder.notifyMsg.setText(status.getContent());

        try {
            Date date = sdf.parse(status.getCreatedDate());
//            calendar.setTime(date);

            viewHolder.date.setText(sdf1.format(date));
            viewHolder.time.setText(sdf2.format(date));
//            viewHolder.date.setText(calendar.get(Calendar.DATE) + " "
//                    + DateFormat.format("MMMM", date) + " " + calendar.get(Calendar.YEAR));
//            viewHolder.time.setText(calendar.get(Calendar.HOUR) + "");
        } catch (ParseException e) {
            Logger.e("created date ex", "parse ex " + e.getMessage());
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView date, time, respondStatus, notifyMsg;

        ViewHolder(final View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.notify_date);
            time = (TextView) itemView.findViewById(R.id.notify_time);
            respondStatus = (TextView) itemView.findViewById(R.id.tv_respond);
            notifyMsg = (TextView) itemView.findViewById(R.id.notify_detail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, StatusDetailActivity.class);
                    intent.putExtra("notification", statusList.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }
}
