package com.emts.alacer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.activity.BCMDetailActivity;
import com.emts.alacer.model.Report;

import java.util.ArrayList;

/**
 * Created by Srijana on 4/26/2017.
 */

public class BCMMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Report> bcmList;
    Context context;
    int currentColorIndex = 0;

    int[] menuColor = new int[]{R.color.bg_emergency, R.color.bg_disaster,
            R.color.bg_business, R.color.bg_crisis_mgnt, R.color.bg_crisis_com, R.color.bg_supply_chain,
            R.color.bg_country, R.color.bg_power};

    public BCMMenuAdapter(ArrayList<Report> bcmList, Context context) {
        this.bcmList = bcmList;
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(
                R.layout.item_bcmplan_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BCMMenuAdapter.ViewHolder viewHolder = (BCMMenuAdapter.ViewHolder) holder;
        Report bcm = bcmList.get(position);
        viewHolder.mTitle.setText(bcm.getTypeName());
        viewHolder.mImg.setBackgroundColor(context.getResources().getColor(menuColor[currentColorIndex % menuColor.length]));
        currentColorIndex++;

    }


    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        View mImg;

        ViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.menu_title);
            mImg = itemView.findViewById(R.id.menu_view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, BCMDetailActivity.class);
                    intent.putExtra("bcm", bcmList.get(getLayoutPosition()));
                    context.startActivity(intent);

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return bcmList.size();
    }


}
