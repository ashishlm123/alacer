package com.emts.alacer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.activity.ReportingDetail;
import com.emts.alacer.activity.superuser.StatusMenuActivity;
import com.emts.alacer.activity.superuser.MassNotificationDetail;
import com.emts.alacer.activity.superuser.NotificationStatusListing;
import com.emts.alacer.model.Report;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Srijana on 4/25/2017.
 */

public class ReportMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Report> rList;
    private boolean isSuperUser;
    private boolean isStatusCheck;

    public ReportMenuAdapter(Context context, ArrayList<Report> rList) {
        this.context = context;
        this.rList = rList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(
                R.layout.item_super_user_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ReportMenuAdapter.ViewHolder viewHolder = (ViewHolder) holder;
        Report report = rList.get(position);

        if (report.getId().equals("-1")){
            viewHolder.mImg.setImageResource(R.drawable.icon_status);
        }else {
            Picasso.with(context).load(report.getTypeImage()).into(viewHolder.mImg);
        }
        viewHolder.mTitle.setText(report.getTypeName());

    }

    @Override
    public int getItemCount() {
        return rList.size();
    }

    public boolean isSuperUser() {
        return isSuperUser;
    }

    public void setSuperUser(boolean superUser) {
        isSuperUser = superUser;
    }

    public void setStatusCheck(boolean statusCheck) {
        isStatusCheck = statusCheck;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        ImageView mImg;

        ViewHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.menu_title);
            mImg = (ImageView) itemView.findViewById(R.id.menu_img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (isSuperUser) {
                        if (isStatusCheck){
                            Intent intent = new Intent(context, NotificationStatusListing.class);
                            intent.putExtra("report", rList.get(getLayoutPosition()));
                            context.startActivity(intent);
                        }else {
                            if (rList.get(getLayoutPosition()).getId().equals("-1")) {
                                Intent intent = new Intent(context, StatusMenuActivity.class);
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, MassNotificationDetail.class);
                                intent.putExtra("report", rList.get(getLayoutPosition()));
                                context.startActivity(intent);
                            }
                        }
                    } else {
                        Intent intent = new Intent(context, ReportingDetail.class);
                        intent.putExtra("report", rList.get(getLayoutPosition()));
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}
