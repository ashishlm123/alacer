package com.emts.alacer.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.activity.DeleteMesgActivty;
import com.emts.alacer.activity.InboxDetailActivity;
import com.emts.alacer.activity.UserMainActivity;
import com.emts.alacer.model.Notification;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Srijana on 4/20/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Notification> mesgList;
    SimpleDateFormat sdf, sdf1;
    private SparseBooleanArray mSelectedItemsIds;
    ColorStateList colorStateList;
    boolean isDelete;


    public NotificationAdapter(Context context, ArrayList<Notification> mesgList) {
        this.context = context;
        this.mesgList = mesgList;
        sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf1 = new SimpleDateFormat("dd MMM yyyy");
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_inbox, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NotificationAdapter.ViewHolder viewHolder = (NotificationAdapter.ViewHolder) holder;
        Notification notification = mesgList.get(position);
        if (!TextUtils.isEmpty(notification.getImageUrl())) {
            Picasso.with(context).load(notification.getImageUrl()).into(viewHolder.icon);
        } else {
            viewHolder.icon.setImageResource(-1);
        }
        viewHolder.title.setText(notification.getTitle());
        viewHolder.mesg.setText(notification.getMesg());

        String date = getFormattedDate(notification.getDate());
        if (date.equals("Today")) {
            viewHolder.date.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            viewHolder.date.setTextColor(viewHolder.mesg.getTextColors());
        }
        viewHolder.date.setText(date);


        if (notification.isSelected()) {
            viewHolder.main.setBackgroundColor(context.getResources().getColor(R.color.grey1));
            viewHolder.tick.setVisibility(View.VISIBLE);
        } else {
            viewHolder.main.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            viewHolder.tick.setVisibility(View.GONE);
        }

    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon, tick;
        TextView date, title, mesg;
        RelativeLayout main;


        ViewHolder(final View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.img_inbox_mesg);
            date = (TextView) itemView.findViewById(R.id.mesg_date);
            title = (TextView) itemView.findViewById(R.id.mesg_title);
            mesg = (TextView) itemView.findViewById(R.id.mesg_detail);
            main = (RelativeLayout) itemView.findViewById(R.id.main);
            tick = (ImageView) itemView.findViewById(R.id.tick);
            TextView status = (TextView) itemView.findViewById(R.id.mesg_status);
            status.setVisibility(View.GONE);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isDelete && clickListener != null) {
                        clickListener.onRecyclerViewItemClick(getLayoutPosition());
                    } else {

                        Intent intent = new Intent(context, InboxDetailActivity.class);
                        intent.putExtra("notification", mesgList.get(getLayoutPosition()));
                        ((UserMainActivity) context).startActivityForResult(intent, UserMainActivity.INBOXDETAIL);
                    }

                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (!isDelete) {
                        Intent intent = new Intent(context, DeleteMesgActivty.class);
                        intent.putExtra("notification", mesgList);
                        intent.putExtra("selecteditem", getLayoutPosition());
                        ((UserMainActivity) context).startActivityForResult(intent, UserMainActivity.INBOXDETAIL);
                    }else{

                    }


                    if (clickListener != null) {
                        clickListener.onRecyclerViewItemLongClick(getLayoutPosition());
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mesgList.size();
    }

    public String getFormattedDate(String createdDate) {
        try {
            Date date = sdf.parse(createdDate);
            if (DateUtils.isToday(date.getTime())) {
                return "Today";
            } else {
                return sdf1.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdDate;
    }


    public OnRecyclerViewItemClicked clickListener;

    public void setRecycleViewItemClickListener(OnRecyclerViewItemClicked clickListener) {
        this.clickListener = clickListener;
    }

    public interface OnRecyclerViewItemClicked {
        void onRecyclerViewItemLongClick(int position);

        void onRecyclerViewItemClick(int position);
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
}
