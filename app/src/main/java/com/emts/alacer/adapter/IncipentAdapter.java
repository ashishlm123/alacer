package com.emts.alacer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.activity.superuser.IncidentReportDetail;
import com.emts.alacer.model.Incident;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Srijana on 4/25/2017.
 */

public class IncipentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Incident> iList;
    SimpleDateFormat sdf, sdf1;

    public IncipentAdapter(Context context, ArrayList<Incident> iList) {
        this.context = context;
        this.iList = iList;
        sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf1 = new SimpleDateFormat("dd MMM yyyy");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_inbox, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IncipentAdapter.ViewHolder viewHolder = (ViewHolder) holder;
        Incident incident = iList.get(position);
        viewHolder.mTitle.setText(incident.getTypeName());

        String date=getFormattedDate(incident.getCreatedDate());
        if(date.equals("Today")){
            viewHolder.mDate.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }else{
            viewHolder.mDate.setTextColor(viewHolder.mDetail.getTextColors());
        }
        viewHolder.mDate.setText(date);
        viewHolder.mDetail.setText(incident.getMesg());
        viewHolder.mStatus.setText(incident.getStatus());

        Picasso.with(context).load(incident.getImageUrl()).into(viewHolder.mImg);


    }

    @Override
    public int getItemCount() {
        return iList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle, mDate, mDetail, mStatus;
        ImageView mImg;

        ViewHolder(final View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.mesg_title);
            mDetail = (TextView) itemView.findViewById(R.id.mesg_detail);
            mDate = (TextView) itemView.findViewById(R.id.mesg_date);
            mStatus = (TextView) itemView.findViewById(R.id.mesg_status);
            mImg = (ImageView) itemView.findViewById(R.id.img_inbox_mesg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, IncidentReportDetail.class);
                    intent.putExtra("incident", iList.get(getLayoutPosition()));
                    context.startActivity(intent);

                }
            });
        }
    }

    public String getFormattedDate(String createdDate) {
        try {
            Date date = sdf.parse(createdDate);
            if (DateUtils.isToday(date.getTime())) {
                return "Today";
            } else {
                return sdf1.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdDate;
    }
}
