package com.emts.alacer.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.emts.alacer.R;


public class RobotoTextView extends AppCompatTextView {

    public RobotoTextView(Context context) {
        super(context);
        if (isInEditMode()) return;
        parseAttributes(null);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }


    private void parseAttributes(AttributeSet attrs) {
        int typeface;
        if (attrs == null) {
            //Not created from xml
        } else {
            TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.RobotoTextView);
            typeface = values.getInt(R.styleable.RobotoTextView_typeface, -1);
            values.recycle();

            if (typeface != -1) {
                setTypeface(getCustomTypeface(getContext(), typeface));
            }
        }
    }

    public static Typeface getCustomTypeface(Context context, int typeface) {
        switch (typeface) {
            case CustomTypeface.OPEN_SANS_REGULAR:
                if (CustomTypeface.openSansRegular == null) {
                    CustomTypeface.openSansRegular = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_regular.ttf");
                }
                return CustomTypeface.openSansRegular;

            case CustomTypeface.OPEN_SANS_BOLD:
                if (CustomTypeface.openSansBold == null) {
                    CustomTypeface.openSansBold = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_bold.ttf");
                }
                return CustomTypeface.openSansBold;

            case CustomTypeface.OPEN_SANS_EXTRA_BOLD:
                if (CustomTypeface.openSansExtraBold == null) {
                    CustomTypeface.openSansExtraBold = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_extrabold.ttf");
                }
                return CustomTypeface.openSansExtraBold;

            case CustomTypeface.HELVETICA:
                if (CustomTypeface.helvetica == null) {
                    CustomTypeface.helvetica = Typeface.createFromAsset(context.getAssets(), "fonts/helvetica_bold.ttf");
                }
                return CustomTypeface.helvetica;

            default:
                return Typeface.DEFAULT;
        }
    }

    private static class CustomTypeface {
        static final int OPEN_SANS_REGULAR = 19;
        static final int OPEN_SANS_BOLD = 20;
        static final int OPEN_SANS_EXTRA_BOLD = 21;
        static final int HELVETICA = 22;

        private static Typeface openSansRegular;
        private static Typeface openSansBold;
        private static Typeface openSansExtraBold;
        private static Typeface helvetica;
    }
}
