package com.emts.alacer;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;

import com.emts.alacer.helper.Logger;

/**
 * Created by User on 2017-04-28.
 */
public class AudioService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    public static final int STATUS_STOP = 0;
    public static final int STATUS_PLAYING = 1;
    public static final int STATUS_PAUSE = 2;
    public int playStatus = STATUS_STOP;
    public int mProgress = 0;
    public int totalLength = 0;

    //media player
    private MediaPlayer player;
    //binder
    private final IBinder audioBinder = new AudioBinder();
    String audioUrl;
    Handler mHandler;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (totalLength > 0 && player.isPlaying()) {
                mProgress = (int) (((float)player.getCurrentPosition() / (float) totalLength) * 100);
                Logger.e("mProgress calculate", player.getDuration() + " Current:" + player.getCurrentPosition());
                mHandler.postDelayed(runnable, 1000);
                Logger.e("service progres", mProgress + " ");
            }
            if (!player.isPlaying()) {
                mHandler.removeCallbacks(runnable);
            }
        }
    };

    public void onCreate() {
        //create the service
        super.onCreate();
        //create player
        player = new MediaPlayer();
        mHandler = new Handler();
        //initialize
        initMusicPlayer();
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public void initMusicPlayer() {
        //set player properties
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //set listeners
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    //binder
    public class AudioBinder extends Binder {
        public AudioService getService() {
            return AudioService.this;
        }
    }

    //activity will bind to service
    @Override
    public IBinder onBind(Intent intent) {
        return audioBinder;
    }

    //release resources when unbind
    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        mProgress = 0;
        playStatus = STATUS_STOP;
        mHandler.removeCallbacks(runnable);
        return false;
    }

    //play a song
    public void playSong() {
        //play
        player.reset();
        //set uri
//        Uri trackUri = ContentUris.withAppendedId(
//                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
//                currSong);
        //set the data source
        try {
            Uri audioUri = Uri.parse(audioUrl);
            player.setDataSource(getApplicationContext(), audioUri);
        } catch (Exception e) {
            Logger.e("MUSIC SERVICE", "Error setting data source :" + e.getMessage());
        }
        player.prepareAsync();

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //check if playback has reached the end of a track
        if (player.getCurrentPosition() > 0) {
            mp.reset();
            player.stop();
//            player.release();
            mProgress = 100;
            playStatus = STATUS_STOP;
            mHandler.removeCallbacks(runnable);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Logger.e("MUSIC PLAYER", "Playback Error");
        mp.reset();
        mProgress = 0;
        playStatus = STATUS_STOP;
        mHandler.removeCallbacks(runnable);
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //start playback
        mp.start();
        totalLength = player.getDuration();
        mProgress = 0;
        playStatus = STATUS_PLAYING;
        mHandler.postDelayed(runnable, 1000);
    }

    public void pausePlayer() {
        player.pause();
        playStatus = STATUS_PAUSE;
    }

    public void seek(int posn) {
        player.seekTo(posn);
    }

    public void go() {
        player.start();
        playStatus = STATUS_PLAYING;
    }

    @Override
    public void onDestroy() {
        mProgress = 0;
        playStatus = STATUS_STOP;
        mHandler.removeCallbacks(runnable);
        stopForeground(true);
    }

}