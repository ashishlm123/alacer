package com.emts.alacer.model;

/**
 * Created by User on 2017-05-09.
 */

public class Status extends Report {
    private String statusId;
    private String departId;
    private String branchId;
    private String totalSend;
    private String totalResponse;

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getTotalSend() {
        return totalSend;
    }

    public void setTotalSend(String totalSend) {
        this.totalSend = totalSend;
    }

    public String getTotalResponse() {
        return totalResponse;
    }

    public void setTotalResponse(String totalResponse) {
        this.totalResponse = totalResponse;
    }
}
