package com.emts.alacer.model;

/**
 * Created by User on 2017-04-26.
 */

public class Branch {
    private String branchName;
    private String branchId;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    @Override
    public String toString() {
        return this.branchName;
    }
}
