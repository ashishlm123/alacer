package com.emts.alacer.model;

/**
 * Created by User on 2017-04-26.
 */

public class Department {
    private String departName;
    private String departId;

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    @Override
    public String toString() {
        return this.departName;
    }
}
