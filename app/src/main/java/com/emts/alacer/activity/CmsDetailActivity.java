package com.emts.alacer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.model.Report;

/**
 * Created by Srijana on 4/25/2017.
 */

public class CmsDetailActivity extends AppCompatActivity {
    Report moreList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cms_detail);

        Intent intent = getIntent();
        if (intent != null) {
            moreList = (Report) intent.getSerializableExtra("more");
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(moreList.getTypeName());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView detail = (TextView) findViewById(R.id.cms_detail);
        detail.setText(Html.fromHtml(moreList.getContent()));

    }


}
