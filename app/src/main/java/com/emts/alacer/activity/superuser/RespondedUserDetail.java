package com.emts.alacer.activity.superuser;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.model.User;

public class RespondedUserDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responded_user_detail);

        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
            return;
        }

        User user = (User) intent.getSerializableExtra("user");
        boolean isResponded = intent.getBooleanExtra("isResponded", false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (isResponded) {
            ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Responded");
        } else {
            ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Not Responded");
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView tvName = (TextView) findViewById(R.id.d_name);
        tvName.setText(user.getName());
        TextView tvMobile = (TextView) findViewById(R.id.d_mobile);
        tvMobile.setText(user.getMobile());
        TextView tvDepart = (TextView) findViewById(R.id.d_depart);
        tvDepart.setText(user.getDepartment());
        TextView tvBranch = (TextView) findViewById(R.id.d_branch);
        tvBranch.setText(user.getBranch());
        TextView tvEmail = (TextView) findViewById(R.id.d_email);
        tvEmail.setText(user.getEmail());

    }
}
