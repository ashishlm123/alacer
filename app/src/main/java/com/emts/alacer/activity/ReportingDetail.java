package com.emts.alacer.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.AlacerApp;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.GPSTracker;
import com.emts.alacer.helper.ImageHelper;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.MultipartUtility;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.Utils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Report;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Srijana on 4/21/2017.
 */

public class ReportingDetail extends AppCompatActivity {
    CircularProgressBar circularProgressBar;
    Report report;
    EditText message, aNDecision;
    TextView location, errorMessage, errorLocation, errorAction, tvTimer;
    SeekBar impact;
    ImageView record, cancel, addMedia;

    int PLACE_PICKER_REQUEST = 1;
    String latitude = "3.1390029999999998";
    String longitude = "101.686855";
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(3.1390029999999998, 101.686855), new LatLng(3.1390029999999998, 101.686855));
    String AudioSavePathInDevice = "null";
    String imagePath = "null";
    String videoPath = "null";
    float impactValue = 0;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;
    public static Uri fileUri;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int RESULT_LOAD_VIDEO = 124;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;
    public static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 9874;


    public static final int REQ_PERMISSIONS_GPS = 957;
    SwitchCompat switchLocation;
    boolean isClose = false;
    boolean isPlay = false;
    boolean isSave = false;
    GPSTracker gpsTracker;
    boolean video = false;
    int retryCount = 0;
    public int seconds = 0;
    public int minutes = 0;
    //Declare the timer
    Timer timer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporting_detail);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.report_toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Reporting");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        circularProgressBar = (CircularProgressBar) findViewById(R.id.cbProgress);

        Intent intent = getIntent();
        report = (Report) intent.getSerializableExtra("report");


        ImageView logo = (ImageView) findViewById(R.id.report_logo);
        if (!TextUtils.isEmpty(report.getTypeImage())) {
            Picasso.with(getApplicationContext()).load(report.getTypeImage()).into(logo);
        }
        TextView title = (TextView) findViewById(R.id.report_title);
        title.setText(report.getTypeName());

        errorMessage = (TextView) findViewById(R.id.validate_message);
        errorLocation = (TextView) findViewById(R.id.validate_location);
        errorAction = (TextView) findViewById(R.id.validate_action_des);
        tvTimer = (TextView) findViewById(R.id.tv_timer);

        addMedia = (ImageView) findViewById(R.id.img_add_media);
        addMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] options = {"Take Photo", "Choose Photo", "Take Video", "Choose Video", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportingDetail.this);
                builder.setTitle("Choose Media");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            if (ContextCompat.checkSelfPermission(ReportingDetail.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(ReportingDetail.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                            } else {
                                takePhoto();
                            }

                        } else if (options[item].equals("Choose Photo")) {

                            if (ContextCompat.checkSelfPermission(ReportingDetail.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(ReportingDetail.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        RESULT_LOAD_IMAGE);

                            } else {
                                video = false;
                                chooseFrmGallery(video);
                            }

                        } else if (options[item].equals("Take Video")) {
                            if (ContextCompat.checkSelfPermission(ReportingDetail.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(ReportingDetail.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
                            } else {
                                takeVideo();
                            }

                        } else if (options[item].equals("Choose Video")) {

                            if (ContextCompat.checkSelfPermission(ReportingDetail.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(ReportingDetail.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        RESULT_LOAD_VIDEO);

                            } else {
                                video = true;
                                chooseFrmGallery(video);
                            }

                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        random = new Random();
        record = (ImageView) findViewById(R.id.img_voice);
        cancel = (ImageView) findViewById(R.id.img_cancel);

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {
                audioTask();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (isClose) {
//                    if (mediaPlayer != null) {
//                        mediaPlayer.stop();
//                        mediaPlayer.release();
//                        MediaRecorderReady();
//                    }
//                    Toast.makeText(ReportingDetail.this, "Audio stopped",
//                            Toast.LENGTH_LONG).show();
//                } else {
//                    saveRecord();
//                }

//                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
//                    mediaPlayer.stop();
//                    mediaPlayer.release();
//                    MediaRecorderReady();
//                }
                isPlay = false;
                isSave = false;
                isClose = false;
                AudioSavePathInDevice = "null";
                seconds = 0;
                minutes = 0;
                tvTimer.setText("0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                cancel.setVisibility(View.GONE);
                Picasso.with(ReportingDetail.this).load((R.drawable.btn_voice)).into(record);
            }
        });

        message = (EditText) findViewById(R.id.message);
        aNDecision = (EditText) findViewById(R.id.action_decision);
        location = (TextView) findViewById(R.id.location);
        switchLocation = (SwitchCompat) findViewById(R.id.switch_location);
        switchLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    if (ContextCompat.checkSelfPermission(ReportingDetail.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(ReportingDetail.this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                REQ_PERMISSIONS_GPS);

                    } else {
                        setLocation();
                    }

                } else {
                    location.setText("Location of incident");
                    location.setClickable(true);
                }
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    startActivityForResult(intentBuilder.build(ReportingDetail.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    Toast.makeText(ReportingDetail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(ReportingDetail.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });


        impact = (SeekBar) findViewById(R.id.seek_impact);
        impact.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                TextView txtImpact = (TextView) findViewById(R.id.txt_impact);

                float decimalProgress = (float) progress / 10;
                impactValue = decimalProgress * 10;
                Logger.e("progress", String.valueOf(impactValue));
                if (progress < 33) {
                    txtImpact.setText("LOW");
                } else if (progress > 33 && progress < 66) {
                    txtImpact.setText("MEDIUM");

                } else {
                    txtImpact.setText("HIGH");
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        Button submit = (Button) findViewById(R.id.btn_submit_incident);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;

                if (TextUtils.isEmpty(message.getText().toString().trim())) {
                    errorMessage.setText("This field is required");
                    errorMessage.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    errorMessage.setVisibility(View.GONE);
                }

                if (location.getText().toString().trim().equals("Location of incident")) {
                    errorLocation.setText("Please select a location");
                    isValid = false;
                    errorLocation.setVisibility(View.VISIBLE);
                } else {
                    errorLocation.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(aNDecision.getText().toString().trim())) {
                    isValid = false;
                    errorAction.setText("This field is required");
                    errorAction.setVisibility(View.VISIBLE);
                } else {
                    errorAction.setVisibility(View.GONE);

                }

                if (isValid) {
                    if (NetworkUtils.isInNetwork(ReportingDetail.this)) {
                        String token = String.valueOf(PreferenceHelper.getInstance(AlacerApp.getInstance().getApplicationContext()).getToken());
                        String apiKey = Api.getInstance().apiKey;


                        reportingIncidentTask(message.getText().toString().trim(), location.getText().toString().trim(),
                                aNDecision.getText().toString().trim(), String.valueOf(impactValue));
//                        new ReportingIncident().execute(Api.getInstance().incidentReportUrl, token,
//                                apiKey, message.getText().toString().trim(), location.getText().toString().trim(),
//                                aNDecision.getText().toString().trim(),
//                                String.valueOf(impactValue));
                    } else {
                        AlertUtils.showSnack(ReportingDetail.this, errorLocation, "No Internet connected!!");
                    }
                } else {
                    AlertUtils.showSnack(ReportingDetail.this, errorLocation, "Please fill valid information");
                }
            }
        });

    }

    private void audioTask() {
        if (checkPermission()) {
            if (isPlay) {
                if (isClose) {
                    if (mediaPlayer != null) {
                        mediaPlayer.stop();
                        Picasso.with(ReportingDetail.this).load((R.drawable.btn_play)).into(record);
                        mediaPlayer.release();
                        MediaRecorderReady();
                        Toast.makeText(ReportingDetail.this, "Audio stopped",
                                Toast.LENGTH_LONG).show();
                        isClose = false;
                        cancel.setVisibility(View.VISIBLE);
                    }
                } else {
                    isClose = true;
                    Picasso.with(ReportingDetail.this).load((R.drawable.btn_hold)).into(record);
                    cancel.setVisibility(View.GONE);
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(AudioSavePathInDevice);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        Log.e("record exception", e.getMessage());
                    }
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            totalLength = mediaPlayer.getDuration();
                            mHandler.post(runnable);
                            circularProgressBar.setVisibility(View.VISIBLE);
                            circularProgressBar.setProgress(0);
                        }
                    });
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            mediaPlayer.stop();
                            Picasso.with(ReportingDetail.this).load((R.drawable.btn_play)).into(record);
                            mediaPlayer.release();
                            MediaRecorderReady();
                            isClose = false;
                            cancel.setVisibility(View.VISIBLE);
                        }
                    });
                    Toast.makeText(ReportingDetail.this, "Recording Playing",
                            Toast.LENGTH_LONG).show();
                }

            } else if (isSave) {
                saveRecord();
            } else {
                MediaRecorderReady();
                Picasso.with(ReportingDetail.this).load((R.drawable.btn_recording)).into(record);
                showTimer();
                File storagePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Alacer");
                storagePath.mkdirs();
                AudioSavePathInDevice = storagePath.getPath() + "/" + CreateRandomAudioFileName(5) + ".3gp";
//                        AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Alacer" + "/" + CreateRandomAudioFileName(5) + ".3gp";
                mediaRecorder.setOutputFile(AudioSavePathInDevice);

                try {
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                } catch (IllegalStateException e) {
                    Logger.e("recordStateEx:", e.getMessage());
                } catch (IOException e) {
                    Logger.e("recordIOEx:", e.getMessage());
                }

//                cancel.setVisibility(View.VISIBLE);
                isSave = true;
                Toast.makeText(ReportingDetail.this, "Recording started",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            requestPermission();
        }
    }

    int totalLength = 0;
    int mProgress = 0;
    Handler mHandler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Logger.e("progress", "u r here");
            circularProgressBar.setVisibility(View.VISIBLE);
            try {
                if (totalLength > 0 && mediaPlayer.isPlaying()) {
                    mProgress = (int) (((float) mediaPlayer.getCurrentPosition() / (float) totalLength) * 100);
                    Logger.e("mProgress calculate", mediaPlayer.getDuration() + " Current:" + mediaPlayer.getCurrentPosition());
                    mHandler.postDelayed(runnable, 1000);
                    Logger.e("service progres", mProgress + " ");
                    circularProgressBar.setProgress(mProgress);
                }
                if (mProgress <= 0 || mProgress >= 100) {
                    circularProgressBar.setVisibility(View.GONE);
                    circularProgressBar.setProgress(0);
                }
                if (!mediaPlayer.isPlaying()) {
                    mHandler.removeCallbacks(runnable);
                }
            } catch (IllegalStateException e) {
                circularProgressBar.setVisibility(View.GONE);
                circularProgressBar.setProgress(0);
                mHandler.removeCallbacks(runnable);
            }
        }
    };

    private void setLocation() {
        boolean isGPSEnabled = false;
        final ProgressDialog progressDialog = new ProgressDialog(ReportingDetail.this);
        progressDialog.setMessage("Please wait .Setting Your current location");


        LocationManager locationManager = (LocationManager) ReportingDetail.this
                .getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isGPSEnabled) {
            progressDialog.show();
            gpsTracker = new GPSTracker(getApplicationContext());
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());

            Geocoder geocoder = new Geocoder(ReportingDetail.this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
                String cityName = addresses.get(0).getLocality();
                location.setText(cityName);
                location.setClickable(false);
                progressDialog.dismiss();
            } catch (IOException e) {
                Logger.e("setLocEx:", e.getMessage());
            } catch (IndexOutOfBoundsException e) {
                if (retryCount < 5) {
                    Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            setLocation();
                            retryCount++;
                            Logger.e("count", String.valueOf(retryCount));
                            progressDialog.dismiss();
                        }
                    };
                    handler.postDelayed(runnable, 3000);
                } else if (retryCount > 5) {
                    progressDialog.dismiss();
                    retryCount = 0;
                    switchLocation.setChecked(false);
                } else {
                    progressDialog.dismiss();
                    retryCount = 0;
                    Logger.e("count2", String.valueOf(retryCount));
                }
                Logger.e("ur loc", latitude + "  " + longitude);
            }

        } else {
            final AlertDialog.Builder alert = new AlertDialog.Builder(ReportingDetail.this);
            alert.setTitle("Message");
            alert.setMessage("Please turn on gps");
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    switchLocation.setChecked(false);
                }
            });
            alert.show();
        }
    }

    private void takeVideo() {
        video = true;
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        if (intent.resolveActivity(getPackageManager()) != null) {
            // start the image capture Intent
            startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
        }


    }


    private void saveRecord() {
        stopTimer();
        mediaRecorder.stop();
        isPlay = true;
        cancel.setVisibility(View.GONE);
        Picasso.with(ReportingDetail.this).load((R.drawable.btn_play)).into(record);
        Toast.makeText(ReportingDetail.this, "Recording Completed",
                Toast.LENGTH_LONG).show();
        cancel.setVisibility(View.VISIBLE);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(ReportingDetail.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    public void stopTimer() {
        try {
            timer.cancel();
            tvTimer.setVisibility(View.GONE);
        } catch (Exception e) {

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    //  task you need to do.
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case RESULT_LOAD_IMAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }


            case CAMERA_CAPTURE_VIDEO_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    video = true;
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // start the image capture Intent
                        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
                    }


                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case RESULT_LOAD_VIDEO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_VIDEO);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case REQ_PERMISSIONS_GPS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setLocation();

                }
                return;
            }

            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(ReportingDetail.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                        audioTask();
                    } else {
                        Toast.makeText(ReportingDetail.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    private String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }

    private void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void showTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        tvTimer.setVisibility(View.VISIBLE);
                        if (seconds < 10 & minutes < 10) {
                            tvTimer.setText("0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                        } else if (minutes < 10) {
                            tvTimer.setText("0" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        } else if (seconds < 10) {
                            tvTimer.setText(String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                        } else {
                            tvTimer.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        }
                        seconds += 1;

                        if (seconds == 60) {
                            if (minutes < 10) {
                                tvTimer.setText("0" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                            } else {
                                tvTimer.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                            }
                            seconds = 00;
                            minutes = minutes + 1;

                        }


                    }

                });
            }

        }, 0, 1000);
    }

    public void chooseFrmGallery(boolean video) {
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }


    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    public Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));

////        Uri photoURI = FileProvider.getUriForFile(ReportingDetail.this,
////                BuildConfig.APPLICATION_ID + ".provider",
////                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(ReportingDetail.this,
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;

    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Alacer");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "Alacer" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
//            mediaFile = new File(mediaStorageDir.getPath() + File.separator
//                    + "VID_" + timeStamp + ".wmv");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }


        return mediaFile;
    }


    private void reportingIncidentTask(String message, String location, String actionDecision, String impact) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ReportingDetail.this, "Sending...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("token", PreferenceHelper.getInstance(ReportingDetail.this).getToken());
        postParam.put("type_id", report.getId());
        postParam.put("location", location);
        postParam.put("message", message);
        postParam.put("action", actionDecision);
        postParam.put("scale", impact);
        postParam.put("latitude", latitude);
        postParam.put("lognitude", longitude);
        postParam.put("user_id", PreferenceHelper.getInstance(ReportingDetail.this).getUserId());
        postParam.put("dummyField", "dummy");

        HashMap<String, String> dataPost = new HashMap<>();

        if (!AudioSavePathInDevice.equals("null")) {
            dataPost.put("incident_voice", AudioSavePathInDevice);
        }
//        dataPost.put("incident_voice", "/storage/emulated/0/Alacer/7 - Haal - E - Dil - [Songspk.CC].mp3");
        if (!imagePath.equals("null")) {
            dataPost.put("incident_image", imagePath);
        }
        if (!videoPath.equals("null")) {
            dataPost.put("incident_video", videoPath);
        }
        Logger.e("i,v,a", imagePath + "\n" + videoPath + "\n" + AudioSavePathInDevice);


        vHelper.addMultipartRequest(Api.getInstance().incidentReportUrl, Request.Method.POST, postParam, dataPost,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {

                        try {
                            JSONObject reObj = new JSONObject(response);
                            if (reObj.getBoolean("status")) {
                                AlertUtils.showAlertMessage(ReportingDetail.this, "Message", reObj.getString("message"));
                                onBackPressed();
                            } else {
                                AlertUtils.showAlertMessage(ReportingDetail.this, "Message", reObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(ReportingDetail.this, errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("reportingIncidentTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "reportingIncidentTask");
    }

    class ReportingIncident extends AsyncTask<String, Integer, String> {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ReportingDetail.this, "Sending...");


        @Override
        protected void onPreExecute() {

            pDialog.show();

            //todo show the update progress bar
            // uploadProgressLayout.setVisibility(View.GONE);
            // imageUploadProgressBar.setProgress(0);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String res = "";
            String image = "";
            String video = "";
            String audio = "";

            String url = strings[0];
            Logger.e("doInBackground UploadFileToServer url", url + "");
            String token = strings[1];
            Logger.e("doInBackground UploadFileToServer header", token + "");
            String apiKey = strings[2];
            Logger.e("doInBackground apiKey", apiKey + "");
            String message = strings[3];
            Logger.e("doInBackground UploadFileToServer message", message + "");
            String location = strings[4];
            Logger.e("doInBackground UploadFileToServer location", location + "");
            String action = strings[5];
            Logger.e("doInBackground action", action + "");
            String scale = strings[6];
            Logger.e("doInBackground scale", scale + "");

            if (!imagePath.equals("null")) {
                image = imagePath;
                Logger.e("doInBackground imagepath", image + "");
            }
            if (!videoPath.equals("null")) {
                video = videoPath;
                Logger.e("doInBackground video", video + "");
            }
            if (!AudioSavePathInDevice.equals("null")) {
                audio = AudioSavePathInDevice;
                Logger.e("doInBackground audio", audio + "");
            }

            try {
                MultipartUtility multipartUtility = new MultipartUtility(url, "UTF-8", token);
                if (!TextUtils.isEmpty(audio)) {
                    multipartUtility.addFilePart("incident_voice", new File(audio));
                }
                if (!TextUtils.isEmpty(image)) {
                    multipartUtility.addFilePart("incident_image", new File(image));
                }
                if (!TextUtils.isEmpty(video)) {
                    multipartUtility.addFilePart("incident_video", new File(video));
                }
                multipartUtility.addFormField("api_key", apiKey);
                multipartUtility.addFormField("token", token);
                multipartUtility.addFormField("type_id", report.getId());
                multipartUtility.addFormField("location", location);
                multipartUtility.addFormField("message", message);
                multipartUtility.addFormField("action", action);
                multipartUtility.addFormField("scale", scale);
                multipartUtility.addFormField("latitude", latitude);
                multipartUtility.addFormField("lognitude", longitude);
                multipartUtility.addFormField("user_id", PreferenceHelper.getInstance(ReportingDetail.this).getUserId());
                multipartUtility.addFormField("dummyField", "dummy");

                if (!TextUtils.isEmpty(audio)) {
                    File file = new File(audio);
                    Logger.e("check File", file.exists() + "ok");
                }
                if (!TextUtils.isEmpty(image)) {
                    File file1 = new File(image);
                    Logger.e("check File", file1.exists() + "ok");
                }
                if (!TextUtils.isEmpty(video)) {
                    File file2 = new File(video);
                    Logger.e("check File", file2.exists() + "ok");
                }


                List<String> response = multipartUtility.finish();

                Logger.e("SERVER REPLIED:", "replied");

                for (String line : response) {
                    res = res + line;
                    Logger.e("response as list", line);
                }

            } catch (IOException e) {
                Logger.e("exception by multipart", e.getMessage() + "");
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            Logger.e("UploadFileToServer", "Response from server: " + result);

            try {
                JSONObject reObj = new JSONObject(result);
                if (reObj.getBoolean("status")) {
                    AlertUtils.showAlertMessage(ReportingDetail.this, "Message", reObj.getString("message"));
                } else {
                    AlertUtils.showAlertMessage(ReportingDetail.this, "Message", reObj.getString("message"));

                }
            } catch (JSONException e) {
                Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
            }
            pDialog.dismiss();

            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            //  imageUploadProgressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            //  imageUploadProgressBar.setProgress(progress[0]);

            // updating percentage value
            //  imageUploadProgress.setText(String.valueOf(progress[0]) + "%");
//            if (progress[0] == 100) {
//                cancelImageUpload.setVisibility(View.GONE);
//            } else {
//                cancelImageUpload.setVisibility(View.VISIBLE);
//            }
            Logger.e("onProgressUpdate img upload", progress[0] + "");
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(data, this);
                location.setText(place.getName());
                Double lat = place.getLatLng().latitude;
                Double longs = place.getLatLng().longitude;

                latitude = lat.toString().trim();
                longitude = longs.toString().trim();


            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                Bitmap bm = ImageHelper.decodeImageFileWithCompression(fileUri.getPath(),
                        (int) Utils.pxFromDp(ReportingDetail.this, 85f), (int) Utils.pxFromDp(ReportingDetail.this, 85f));
                addMedia.setImageBitmap(bm);
                imagePath = fileUri.getPath();
                Logger.e("imagePath 2", imagePath);
            } else if (requestCode == RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Logger.e("imagePath 2", selectedImagePath);

                Bitmap bm = ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                        (int) Utils.pxFromDp(ReportingDetail.this, 85f), (int) Utils.pxFromDp(ReportingDetail.this, 85f));
                addMedia.setImageBitmap(bm);
                imagePath = selectedImagePath;


            } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
                addMedia.setImageBitmap(createVideoThumbNail(fileUri.getPath()));
                videoPath = fileUri.getPath();
                Logger.e("videopath", videoPath);
            } else if (requestCode == RESULT_LOAD_VIDEO) {
                Uri selectedVideoUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedVideoUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedVideoPath = cursor.getString(column_index);
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(selectedVideoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
                addMedia.setImageBitmap(bMap);
                videoPath = selectedVideoPath;
            }
        }

    }

    public Bitmap createVideoThumbNail(String path) {
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
    }


}
