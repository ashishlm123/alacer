package com.emts.alacer.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.alacer.AudioService;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.model.Notification;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

public class MessageDetailActivity extends AppCompatActivity {

    Notification notification;
    CircularProgressBar circularProgressBar;
    AudioService mService;
    ImageView audioPlay;
    Handler mHandler;
    boolean playing = false;
    boolean fromPush;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.inbox_toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Inbox");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
        } else {
            notification = (Notification) intent.getSerializableExtra("notification");
            fromPush = intent.getBooleanExtra("from_push", false);
        }

        mHandler = new Handler();
        ImageView typeImage = (ImageView) findViewById(R.id.type_image);
        if (!TextUtils.isEmpty(notification.getImageUrl())) {
            Picasso.with(this).load(notification.getImageUrl()).into(typeImage);
        }

        TextView nTypeName = (TextView) findViewById(R.id.type_name);
        nTypeName.setText(notification.getTitle());
        TextView nDetail = (TextView) findViewById(R.id.noti_detail);
        nDetail.setText(notification.getMesg());

        circularProgressBar = (CircularProgressBar) findViewById(R.id.cbProgress);
//        circularProgressBar.setProgress(30);
        audioPlay = (ImageView) findViewById(R.id.audio_play);
        RelativeLayout holderAudio = (RelativeLayout) findViewById(R.id.holder_audio);
        holderAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mService != null && !TextUtils.isEmpty(notification.getAudioUrl())) {
                    if (playing) {
                        audioPlay.setImageResource(R.drawable.btn_play);
                        mService.pausePlayer();
                        playing = false;
                        AlertUtils.showToast(getApplicationContext(), "Audio stopped");
                    } else {
                        audioPlay.setImageResource(R.drawable.btn_hold);
                        mService.setAudioUrl(notification.getAudioUrl());
                        mService.playSong();
                        playing = true;
                        AlertUtils.showToast(getApplicationContext(), "Audio playing");
                    }
                } else {
                    AlertUtils.showSnack(MessageDetailActivity.this, view, "No Audio Available");
                }

            }
        });

        if (TextUtils.isEmpty(notification.getAudioUrl()) || notification.getAudioUrl().equalsIgnoreCase("null")) {
            holderAudio.setVisibility(View.GONE);
        }
    }

    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) service;
            //get service
            mService = binder.getService();
            mService.setAudioUrl(notification.getAudioUrl());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

//    @Override
//    protected void onStart() {
//        super.onStart();
//        Intent playIntent = new Intent(this, AudioService.class);
//        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//        startService(playIntent);
//    }


    @Override
    public void onBackPressed() {
        if (fromPush) {
            Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent playIntent = new Intent(this, AudioService.class);
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//        startService(playIntent);
        mHandler.postDelayed(runnable, 1000);
    }

    @Override
    protected void onPause() {
        try {
            unbindService(musicConnection);
            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
        }
        super.onPause();
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mService != null) {
                int progress = mService.mProgress;
                if (progress > 100) {
                    circularProgressBar.setProgress(100);
                } else if (progress < 0) {
                    circularProgressBar.setProgress(0);
                } else {
                    circularProgressBar.setProgress(progress);
                }
                Logger.e("progress update", mService.mProgress + " ");
                if (circularProgressBar.getProgress() == 100) {
                    audioPlay.setImageResource(R.drawable.btn_play);
                    circularProgressBar.setProgress(0);
                    playing = false;
                    mService.mProgress = 0;
                }

                mHandler.postDelayed(runnable, 1000);
            }
        }
    };
}
