package com.emts.alacer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.activity.superuser.IncidentReport;
import com.emts.alacer.activity.superuser.NotificationStatusListing;
import com.emts.alacer.activity.superuser.SuperUserDashboard;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 4/19/2017.
 */

public class LoginActivity extends AppCompatActivity {
    boolean user;
    EditText etId, etMobile, etPassword;
    TextView errorId, errorMobile, errorPassword;
    PreferenceHelper prefHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = getIntent();
        if (intent != null) {
            user = intent.getBooleanExtra("user", false);
        }
        prefHelper = PreferenceHelper.getInstance(this);

        etId = (EditText) findViewById(R.id.super_user_id);
        etMobile = (EditText) findViewById(R.id.et_mobileno);
        etPassword = (EditText) findViewById(R.id.password);
        etPassword.setTypeface(etMobile.getTypeface());
        errorId = (TextView) findViewById(R.id.error_id);
        errorMobile = (TextView) findViewById(R.id.error_mobile);
        errorPassword = (TextView) findViewById(R.id.error_password);

        TextView forgotPassword = (TextView) findViewById(R.id.forgot_password);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });

        LinearLayout holderSignUp = (LinearLayout) findViewById(R.id.ll_signup);
        TextView signUp = (TextView) findViewById(R.id.sign_up);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
            }
        });

        Button login = (Button) findViewById(R.id.btn_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user) {
                    boolean isValid = true;

                    if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
                        isValid = false;
                        errorMobile.setText("This field is required");
                        errorMobile.setVisibility(View.VISIBLE);
                    } else {
                        errorMobile.setVisibility(View.GONE);
                    }
                    if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                        isValid = false;
                        errorPassword.setText("This field is required");
                        errorPassword.setVisibility(View.VISIBLE);
                    } else {
                        errorPassword.setVisibility(View.GONE);
                    }
                    if (isValid) {
                        if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                            userLoginTask(etMobile.getText().toString().trim(), etPassword.getText().toString().trim());
                        } else {
                            AlertUtils.showSnack(LoginActivity.this, errorMobile, "No Internet Connected !!");
                        }
                    } else {
                        AlertUtils.showSnack(LoginActivity.this, errorMobile, "Please fill the Valid information");
                    }

                } else {
                    boolean isValid = true;

                    if (TextUtils.isEmpty(etId.getText().toString().trim())) {
                        isValid = false;
                        errorId.setText("This field is required");
                        errorId.setVisibility(View.VISIBLE);
                    } else {
                        errorId.setVisibility(View.GONE);
                    }
                    if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                        isValid = false;
                        errorPassword.setText("This field is required");
                        errorPassword.setVisibility(View.VISIBLE);
                    } else {
                        errorPassword.setVisibility(View.GONE);
                    }


                    if (isValid) {
                        if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                            superUserLoginTask(etId.getText().toString().trim(), etPassword.getText().toString().trim());
                        } else {
                            AlertUtils.showSnack(LoginActivity.this, errorMobile, "No Internet Connected !!");
                        }
                    } else {
                        AlertUtils.showSnack(LoginActivity.this, errorMobile, "Please fill the Valid information");
                    }
//                    Intent intent = new Intent(getApplicationContext(), SuperUserDashboard.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
                }
            }
        });

        RelativeLayout holderSuperUserId = (RelativeLayout) findViewById(R.id.rl_s_id);
        RelativeLayout holderMobileNo = (RelativeLayout) findViewById(R.id.rl_mobile_no);
        TextView labelSuperUser = (TextView) findViewById(R.id.label_super_user);
        if (!user) {
            forgotPassword.setVisibility(View.GONE);
            holderSignUp.setVisibility(View.GONE);
            holderSuperUserId.setVisibility(View.VISIBLE);
            holderMobileNo.setVisibility(View.GONE);
            labelSuperUser.setVisibility(View.VISIBLE);
        } else {
            holderSignUp.setVisibility(View.VISIBLE);
            forgotPassword.setVisibility(View.VISIBLE);
            holderSuperUserId.setVisibility(View.GONE);
            holderMobileNo.setVisibility(View.VISIBLE);
            labelSuperUser.setVisibility(View.GONE);
        }

    }

    private void superUserLoginTask(String id, String password) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "SuperUser Logging...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("id_number", id);
        postParams.put("password", password);
        String pushId = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushId)) {
            postParams.put("push_id", pushId);
        }
        vHelper.addVolleyRequestListeners(Api.getInstance().superUserLoginUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                if (resObj.getString("login").equalsIgnoreCase("yes")) {
                                    JSONObject userObj = resObj.getJSONObject("user_detail");
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_ID, userObj.getString("id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("name")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_MOBILE, userObj.getString("mobile_no")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_DEPART_ID, userObj.getString("department_id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_BRANCH_ID, userObj.getString("branch_id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                                    if (userObj.getString("user_type").equals("1")) {
                                        prefHelper.edit().putString(PreferenceHelper.APP_USER_TYPE, PreferenceHelper.APP_USER_SUPER_USER).commit();
                                    } else {
                                        prefHelper.edit().putString(PreferenceHelper.APP_USER_TYPE, PreferenceHelper.APP_USER_USER).commit();
                                    }
                                    prefHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();

                                    Intent intent = new Intent(getApplicationContext(), SuperUserDashboard.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    if (prefHelper.getBoolean(PreferenceHelper.PENDING_SUPERUSER_NOTIFICATION, false)) {
                                        Intent intent1 = new Intent(getApplicationContext(), IncidentReport.class);
                                        startActivity(intent1);
                                        prefHelper.edit().putBoolean(PreferenceHelper.PENDING_SUPERUSER_NOTIFICATION, false).commit();
                                    } else if (prefHelper.getBoolean(PreferenceHelper.PENDING_SUPERUSER_NOTIFICATION_FROM_ADMIN, false)) {
//                                        Intent intent2 = new Intent(getApplicationContext(), NotificationStatusListing.class);
//                                        startActivity(intent2);
//
                                        prefHelper.edit().putBoolean(PreferenceHelper.PENDING_SUPERUSER_NOTIFICATION_FROM_ADMIN, false).commit();
                                    }
                                } else {
                                    AlertUtils.showAlertMessage(LoginActivity.this, "Login Error !!!", resObj.getString("message"));
                                }
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this, "Login Error !!!", resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("registrationTask ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showAlertMessage(LoginActivity.this, "Login Error !!!", errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("superUserLoginTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "superUserLoginTask");
    }

    private void userLoginTask(String mobile, String password) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "User Logging ...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("mobile_no", mobile);
        postParams.put("password", password);
        String pushId = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushId)) {
            postParams.put("push_id", pushId);
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                if (resObj.getString("login").equalsIgnoreCase("yes")) {
                                    JSONObject userObj = resObj.getJSONObject("user_detail");
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_ID, userObj.getString("id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("name")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_MOBILE, userObj.getString("mobile_no")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_DEPART_ID, userObj.getString("department_id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_BRANCH_ID, userObj.getString("branch_id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                                    if (userObj.getString("user_type").equals("1")) {
                                        prefHelper.edit().putString(PreferenceHelper.APP_USER_TYPE, PreferenceHelper.APP_USER_SUPER_USER).commit();
                                    } else {
                                        prefHelper.edit().putString(PreferenceHelper.APP_USER_TYPE, PreferenceHelper.APP_USER_USER).commit();
                                    }
                                    prefHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();

                                    Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    if (prefHelper.getBoolean(PreferenceHelper.PENDING_USER_NOTIFICATION, false)) {
                                        intent.putExtra("pending_notification", true);
                                        prefHelper.edit().putBoolean(PreferenceHelper.PENDING_USER_NOTIFICATION, false).commit();

                                    }
                                    startActivity(intent);
                                } else {
                                    AlertUtils.showAlertMessage(LoginActivity.this, "Login Error !!!", resObj.getString("message"));
                                }
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this, "Login Error !!!", resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("registrationTask ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showAlertMessage(LoginActivity.this, "Login Error !!!", errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("userLoginTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "userLoginTask");
    }
}
