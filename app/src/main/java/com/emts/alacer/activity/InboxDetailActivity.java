package com.emts.alacer.activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.AudioService;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Notification;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 4/20/2017.
 */

public class InboxDetailActivity extends AppCompatActivity {
    CircularProgressBar circularProgressBar;
    Notification notification;
    CheckBox rbAtWork, rbAwayWork, rbNeedAssistance;
    AudioService mService;
    ImageView audioPlay;
    Handler mHandler;
    CardView cardAtWork, cardAwayWork, cardAssistance;
    boolean playing = false;
    boolean fromPush;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.inbox_toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Inbox");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
        } else {
            notification = (Notification) intent.getSerializableExtra("notification");
            fromPush = intent.getBooleanExtra("from_push", false);
        }

        mHandler = new Handler();

        ImageView typeImage = (ImageView) findViewById(R.id.type_image);
        if (!TextUtils.isEmpty(notification.getImageUrl())) {
            Picasso.with(this).load(notification.getImageUrl()).into(typeImage);
        }
        circularProgressBar = (CircularProgressBar) findViewById(R.id.cbProgress);
//        circularProgressBar.setProgress(30);
        audioPlay = (ImageView) findViewById(R.id.audio_play);
        RelativeLayout holderAudio = (RelativeLayout) findViewById(R.id.holder_audio);
        if (TextUtils.isEmpty(notification.getAudioUrl())) {
            holderAudio.setVisibility(View.GONE);
        } else {
            holderAudio.setVisibility(View.VISIBLE);
        }
        holderAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mService != null && !TextUtils.isEmpty(notification.getAudioUrl())) {
                    if (playing) {
                        audioPlay.setImageResource(R.drawable.btn_play);
                        mService.pausePlayer();
                        playing = false;
                        AlertUtils.showToast(getApplicationContext(), "Audio stopped");
                    } else {
                        audioPlay.setImageResource(R.drawable.btn_hold);
                        mService.setAudioUrl(notification.getAudioUrl());
                        mService.playSong();
                        playing = true;
                        AlertUtils.showToast(getApplicationContext(), "Audio playing");
                    }
                } else {
                    AlertUtils.showSnack(InboxDetailActivity.this, view, "No Audio Available");
                }

            }
        });


        TextView nTypeName = (TextView) findViewById(R.id.type_name);
        nTypeName.setText(notification.getTitle());
        TextView nDetail = (TextView) findViewById(R.id.noti_detail);
        nDetail.setText(notification.getMesg());

        rbAtWork = (CheckBox) findViewById(R.id.radio_at_work);
        rbAwayWork = (CheckBox) findViewById(R.id.radio_away_work);
        rbNeedAssistance = (CheckBox) findViewById(R.id.radio_need_assistance);
        rbAtWork.setOnCheckedChangeListener(rbListener);
        rbAwayWork.setOnCheckedChangeListener(rbListener);
        rbNeedAssistance.setOnCheckedChangeListener(rbListener);
        cardAtWork = (CardView) findViewById(R.id.card_at_work);
        cardAwayWork = (CardView) findViewById(R.id.card_away_work);
        cardAssistance = (CardView) findViewById(R.id.card_assistance);
        cardAtWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbAtWork.setChecked(true);
            }
        });
        cardAwayWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbAwayWork.setChecked(true);
            }
        });
        cardAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbNeedAssistance.setChecked(true);
            }
        });

        Button btnRespond = (Button) findViewById(R.id.send);
        btnRespond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbNeedAssistance.isChecked() || rbAwayWork.isChecked() || rbAtWork.isChecked()) {
                    if (NetworkUtils.isInNetwork(InboxDetailActivity.this)) {
                        responseToNotificationTask(view);
                    } else {
                        AlertUtils.showSnack(InboxDetailActivity.this, view, "No Internet Connected !!!");
                    }
                } else {
                    AlertUtils.showSnack(InboxDetailActivity.this, view, "Please select your status");
                }
            }
        });

        if (notification.isResponded()) {
            btnRespond.setVisibility(View.GONE);
            cardAssistance.setClickable(false);
            cardAtWork.setClickable(false);
            cardAwayWork.setClickable(false);
            if (notification.getRespondType().equalsIgnoreCase("safe at work")) {
                rbAtWork.setChecked(true);
                rbAtWork.setClickable(false);
                rbAwayWork.setVisibility(View.GONE);
                rbNeedAssistance.setVisibility(View.GONE);
            } else if (notification.getRespondType().equalsIgnoreCase("safe away from work")) {
                rbAwayWork.setChecked(true);
                rbAtWork.setVisibility(View.GONE);
                rbAwayWork.setClickable(false);
                rbNeedAssistance.setVisibility(View.GONE);
            } else if (notification.getRespondType().equalsIgnoreCase("need assistance")) {
                rbNeedAssistance.setChecked(true);
                rbAtWork.setVisibility(View.GONE);
                rbNeedAssistance.setClickable(false);
                rbAwayWork.setVisibility(View.GONE);
            }
        } else {
            btnRespond.setVisibility(View.VISIBLE);
        }
    }

    private void responseToNotificationTask(final View view) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(InboxDetailActivity.this, "Updating status...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();
        String responseType = "";
        if (rbAtWork.isChecked()) {
            responseType = "safe at work";
        } else if (rbNeedAssistance.isChecked()) {
            responseType = "need assistance";
        } else if (rbAwayWork.isChecked()) {
            responseType = "safe away from work";
        }
        postParams.put("response_type", responseType);
        postParams.put("report_id", notification.getId());
        postParams.put("incident_id", notification.getIncident_id());
        postParams.put("mobile_no", PreferenceHelper.getInstance(InboxDetailActivity.this)
                .getString(PreferenceHelper.APP_USER_MOBILE, ""));

        vHelper.addVolleyRequestListeners(Api.getInstance().notificationResponseUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                view.setVisibility(View.GONE);

                                if (rbNeedAssistance.isChecked()) {
                                    needAssistanceTask();
                                }
                                AlertUtils.showSnack(InboxDetailActivity.this, view, resObj.getString("message"));
                                setResult(RESULT_OK);
                            } else {
                                AlertUtils.showAlertMessage(InboxDetailActivity.this, "Error !!!", resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("response ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showAlertMessage(InboxDetailActivity.this, "Error !!!", errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("response err ex", e.getMessage() + "");
                        }
                        pDialog.dismiss();
                    }
                }, "responseToNotificationTask");
    }

    private void needAssistanceTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(InboxDetailActivity.this, "Notify to comander...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("branch_id", PreferenceHelper.getInstance(InboxDetailActivity.this).getString(PreferenceHelper.APP_USER_BRANCH_ID, ""));
        postParams.put("department_id", PreferenceHelper.getInstance(InboxDetailActivity.this).getString(PreferenceHelper.APP_USER_DEPART_ID, ""));
        vHelper.addVolleyRequestListeners(Api.getInstance().needResponseUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {

                                Intent intent = new Intent(getApplicationContext(), InboxNeedAssistanceActivity.class);
                                intent.putExtra("notification", notification);
                                intent.putExtra("assistance_response", response);
                                startActivity(intent);
                            } else {
                                AlertUtils.showAlertMessage(InboxDetailActivity.this, "Error !!!", resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("response ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showAlertMessage(InboxDetailActivity.this, "Error !!!", errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("response err ex", e.getMessage() + "");
                        }
                        pDialog.dismiss();
                    }
                }, "needAssistanceTask");
    }

    CompoundButton.OnCheckedChangeListener rbListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                if (compoundButton.getId() == R.id.radio_at_work) {
                    rbAtWork.setChecked(true);
                    rbAwayWork.setChecked(false);
                    rbNeedAssistance.setChecked(false);
                } else if (compoundButton.getId() == R.id.radio_away_work) {
                    rbAtWork.setChecked(false);
                    rbAwayWork.setChecked(true);
                    rbNeedAssistance.setChecked(false);
                } else if (compoundButton.getId() == R.id.radio_need_assistance) {
                    rbAtWork.setChecked(false);
                    rbAwayWork.setChecked(false);
                    rbNeedAssistance.setChecked(true);
                }
            }
        }
    };

    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) service;
            //get service
            mService = binder.getService();
            mService.setAudioUrl(notification.getAudioUrl());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

//    @Override
//    protected void onStart() {
//        super.onStart();
//        Intent playIntent = new Intent(this, AudioService.class);
//        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//        startService(playIntent);
//    }


    @Override
    protected void onResume() {
        super.onResume();
        Intent playIntent = new Intent(this, AudioService.class);
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//        startService(playIntent);
        mHandler.postDelayed(runnable, 1000);
    }

    @Override
    protected void onPause() {
        try {
            unbindService(musicConnection);
            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        try {
            unbindService(musicConnection);
            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
        }

        if (fromPush) {
            Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return;
        }
        super.onBackPressed();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mService != null) {
                int progress = mService.mProgress;
                if (progress > 100) {
                    circularProgressBar.setProgress(100);
                } else if (progress < 0) {
                    circularProgressBar.setProgress(0);
                } else {
                    circularProgressBar.setProgress(progress);
                }
                Logger.e("progress update", mService.mProgress + " ");
                if (circularProgressBar.getProgress() == 100) {
                    audioPlay.setImageResource(R.drawable.btn_play);
                    circularProgressBar.setProgress(0);
                    playing = false;
                    mService.mProgress = 0;
                }

                mHandler.postDelayed(runnable, 1000);
            }
        }
    };
}
