package com.emts.alacer.activity.superuser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.IncipentAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Incident;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Srijana on 4/25/2017.
 */

public class IncidentReport extends AppCompatActivity {
    ProgressBar progressBar;
    TextView errorText;
    RecyclerView incidentList;
    IncipentAdapter adapter;
    ArrayList<Incident> iList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_report);


        iList = new ArrayList<>();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Incident Report");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progress);
        errorText = (TextView) findViewById(R.id.error_mesg);

        incidentList = (RecyclerView) findViewById(R.id.inbox_item_list);
        adapter = new IncipentAdapter(this, iList);
        incidentList.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        incidentList.setLayoutManager(linearLayoutManager);

        if (NetworkUtils.isInNetwork(this)) {
            getIncidentListTask();
            incidentList.setVisibility(View.GONE);
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            incidentList.setVisibility(View.GONE);
        }
    }

    private void getIncidentListTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        final HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().incidentListSuperUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        Logger.printLongLog("response",response);
                        try {
                            JSONObject resObj = new JSONObject(response);

                            if (resObj.getBoolean("status")) {
                                JSONArray incidentArray = null;
                                try {
                                    incidentArray = resObj.getJSONArray("incident_list");
                                } catch (JSONException e) {
                                    errorText.setText(resObj.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    incidentList.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                    return;
                                }

                                for (int i = 0; i < incidentArray.length(); i++) {
                                    JSONObject eachObj = incidentArray.getJSONObject(i);
                                    Incident incident = new Incident();
                                    incident.setId(eachObj.getString("id"));
                                    incident.setTypeId(eachObj.getString("type_id"));


                                    if (!TextUtils.isEmpty(eachObj.getString("type_image"))) {
                                        incident.setImageUrl(resObj.getString("icon_path") + "/" + eachObj.getString("type_image"));
                                    }
                                    if (!TextUtils.isEmpty(eachObj.getString("file_saved_image"))) {
                                        incident.setMedia(resObj.getString("icon_path") + "/" + eachObj.getString("file_saved_image"));
                                    }
                                    if (!TextUtils.isEmpty(eachObj.getString("file_saved_voice"))) {
                                        incident.setAudioUrl(resObj.getString("icon_path") + "/" + eachObj.getString("file_saved_voice"));
//                                        incident.setAudioUrl("http://nepaimpressions.com/dev/alacer/themes/admin//images//incidents/sample.mp3");
                                    }

                                    incident.setMesg(eachObj.getString("message"));
                                    incident.setScale(eachObj.getString("scale"));
                                    incident.setStatus(eachObj.getString("status"));
                                    incident.setTypeName(eachObj.getString("type_name"));

                                    String str = eachObj.getString("created_date");
                                    String[] my_date_time = null;
                                    my_date_time = str.split(" ");
                                    String Date_str = my_date_time[0];
                                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(Date_str);
                                    String formattedDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
                                    String Time_str = " / " + my_date_time[1];
                                    incident.setCreatedDate(eachObj.getString("created_date"));
                                    incident.setLaunchedDate(eachObj.getString("launched_date"));
                                    incident.setName(eachObj.getString("action"));
                                    incident.setLocation(eachObj.getString("location"));
                                    incident.setReportedBy(eachObj.getString("reported_by"));
                                    incident.setTypeId(eachObj.getString("type_id"));
                                    incident.setLatitude(eachObj.getString("latitude"));
                                    incident.setLongitude(eachObj.getString("lognitude"));
                                    iList.add(incident);
                                }
                                adapter.notifyDataSetChanged();
                                incidentList.setVisibility(View.VISIBLE);

                            } else {
                                AlertUtils.showToast(IncidentReport.this, resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("reportListingTask ex", e.getMessage() + "");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(IncidentReport.this, errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("getIncidentListTask error ex", e.getMessage() + " ");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                }, "getIncidentListTask");
    }
}
