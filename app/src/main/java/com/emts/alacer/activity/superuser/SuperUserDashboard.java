package com.emts.alacer.activity.superuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.emts.alacer.R;
import com.emts.alacer.activity.UserSelectActivity;
import com.emts.alacer.fragment.Reporting;
import com.emts.alacer.helper.PreferenceHelper;

public class SuperUserDashboard extends AppCompatActivity {
//    String[] menuTitle = new String[]{"ANNOUNCEMENT", "POWER OUTAGES", "CYBER ATTACK", "FIRE",
//            "WEATHER", "SECURITY", "ACCIDENT/INJURY", "OTHER INCIDENT", "STATUS"};
//    int[] menuIcon = new int[]{R.drawable.icon_announcement, R.drawable.icon_poweroutages, R.drawable.icon_cyberattack,
//            R.drawable.icon_fire, R.drawable.icon_weather, R.drawable.icon_security, R.drawable.icon_accident,
//            R.drawable.icon_otherincident, R.drawable.icon_status};
    PreferenceHelper preferenceHelper;
    FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_user_dashboard);
        preferenceHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(0);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Mass Notification");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), IncidentReport.class));
            }
        });
        toolbar.inflateMenu(R.menu.superuser_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                preferenceHelper.edit().clear().commit();
                Intent intent1 = new Intent(getApplicationContext(), UserSelectActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                return false;
            }
        });
        fm = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putBoolean("is_superuser", true);
        Fragment fragInfo = new Reporting();
        fragInfo.setArguments(bundle);
        fm.beginTransaction().replace(R.id.container, fragInfo).commit();

//        RecyclerView superMenuList = (RecyclerView) findViewById(R.id.super_user_menu);
//        superMenuList.setLayoutManager(new LinearLayoutManager(this));
//        superMenuList.setAdapter(new SuperUserMenuAdapter());
    }
//
//    private class SuperUserMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//        @Override
//        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            return new ViewHolder(LayoutInflater.from(SuperUserDashboard.this).inflate(
//                    R.layout.item_super_user_menu, parent, false));
//        }
//
//        @Override
//        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            ViewHolder viewHolder = (ViewHolder) holder;
//            viewHolder.mImg.setImageResource(menuIcon[position]);
//            viewHolder.mTitle.setText(menuTitle[position]);
//        }
//
//        @Override
//        public int getItemCount() {
//            return menuTitle.length;
//        }
//
//        private class ViewHolder extends RecyclerView.ViewHolder {
//            TextView mTitle;
//            ImageView mImg;
//
//            ViewHolder(View itemView) {
//                super(itemView);
//
//                mTitle = (TextView) itemView.findViewById(R.id.menu_title);
//                mImg = (ImageView) itemView.findViewById(R.id.menu_img);
//
//                itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        Toast.makeText(SuperUserDashboard.this, menuTitle[getLayoutPosition()], Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(getApplicationContext(), MassNotificationDetail.class);
//                        startActivity(intent);
//
//
//
//
//                    }
//                });
//            }
//        }
//    }
}
