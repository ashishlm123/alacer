package com.emts.alacer.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.NotificationAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Notification;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 5/18/2017.
 */

public class DeleteMesgActivty extends AppCompatActivity {

    ArrayList<Notification> mesList;
    ArrayList<Notification> mList;
    RecyclerView mesListView;
    NotificationAdapter adapter;
    int selectedlayout;
    ImageView back, delete;
    TextView count;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deletemesg);


        mesList = new ArrayList<>();

        Intent intent = getIntent();
        if (intent != null) {
            mList = (ArrayList<Notification>) intent.getSerializableExtra("notification");
            selectedlayout = intent.getIntExtra("selecteditem", 0);
            mList.get(selectedlayout).setSelected(true);
            mesList.addAll(mList);
        }

        back = (ImageView) findViewById(R.id.back_arrow);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        delete = (ImageView) findViewById(R.id.del);

        count = (TextView) findViewById(R.id.selected_num);
        count.setText("1");
        mesListView = (RecyclerView) findViewById(R.id.mesg_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mesListView.setLayoutManager(linearLayoutManager);

        adapter = new NotificationAdapter(this, mesList);
        adapter.setIsDelete(true);
        mesListView.setAdapter(adapter);
        adapter.setRecycleViewItemClickListener(new NotificationAdapter.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerViewItemLongClick(int position) {

            }

            @Override
            public void onRecyclerViewItemClick(int position) {
                mesList.get(position).setSelected(!mesList.get(position).isSelected());
                int selectCount = 0;
                for (int i = 0; i < mesList.size(); i++) {
                    if (mesList.get(i).isSelected()) {
                        selectCount++;
                    }
                }
                if (selectCount > 0) {
                    count.setText(String.valueOf(selectCount));
                    adapter.notifyDataSetChanged();
                } else {
                    onBackPressed();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(DeleteMesgActivty.this);
                alert.setMessage("Are you sure ?");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteMesgTask();
                        dialogInterface.dismiss();
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        onBackPressed();
                        dialogInterface.dismiss();
                    }
                });
                alert.show();
            }
        });


    }

    private void deleteMesgTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(DeleteMesgActivty.this, "Deleting..");
        pDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();

        for (int i = 0; i < mesList.size(); i++) {
            if (mesList.get(i).isSelected()) {
                postParams.put("message_id[" + i + "]", mesList.get(i).getId());
            }
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().deleteUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {

                            JSONObject resObj = new JSONObject(response);
                            mList.clear();
                            mList.addAll(mesList);
                            mesList.clear();
                            for (int i = 0; i < mList.size(); i++) {

                                if (!mList.get(i).isSelected()) {
                                    mesList.add(mList.get(i));
                                }

                            }
                            adapter.notifyDataSetChanged();
                            count.setText("");
                            setResult(RESULT_OK);

                            AlertUtils.showAlertMessage(DeleteMesgActivty.this, "Message", resObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("deleteMesgTask ex", e.getMessage() + "");
                        }
                        pDialog.dismiss();
                        onBackPressed();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showSnack(DeleteMesgActivty.this, delete, errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("deleteMesgTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "deleteMesgTask");
    }
}
