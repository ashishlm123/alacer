package com.emts.alacer.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.fragment.BCMPlan;
import com.emts.alacer.fragment.Home;
import com.emts.alacer.fragment.Inbox;
import com.emts.alacer.fragment.More;
import com.emts.alacer.fragment.Reporting;
import com.emts.alacer.helper.Logger;

/**
 * Created by Srijana on 4/19/2017.
 */

public class UserMainActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout homeHolder, inboxHolder, bcmPlanHolder, reportingHolder, moreHolder;
    FragmentManager fm;
    View previousSelectedBottomMenu;
    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView toolbarSearchIcon;
    public static  int INBOXDETAIL=12;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        fm = getSupportFragmentManager();

        toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("ALACER");
        toolbarSearchIcon = (ImageView) toolbar.findViewById(R.id.search_icon);
        toolbarSearchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BCMPlan) fm.findFragmentById(R.id.container)).dataTansfer();
            }
        });

        homeHolder = (LinearLayout) findViewById(R.id.lay_home);
        inboxHolder = (LinearLayout) findViewById(R.id.lay_inbox);
        bcmPlanHolder = (LinearLayout) findViewById(R.id.lay_bcm_plans);
        reportingHolder = (LinearLayout) findViewById(R.id.lay_reporting);
        moreHolder = (LinearLayout) findViewById(R.id.lay_more);

        homeHolder.setOnClickListener(this);
        inboxHolder.setOnClickListener(this);
        reportingHolder.setOnClickListener(this);
        moreHolder.setOnClickListener(this);
        bcmPlanHolder.setOnClickListener(this);

        //home
        ImageView homeIcon = (ImageView) homeHolder.findViewById(R.id.nav_icon);
        homeIcon.setImageResource(R.drawable.ic_home_black_24dp);
        TextView homeTitle = (TextView) homeHolder.findViewById(R.id.nav_title);
        homeTitle.setText("ALACER");

        //inbox
        ((ImageView) inboxHolder.findViewById(R.id.nav_icon)).setImageResource(R.drawable.ic_mail_black_envelope_symbol);
        ((TextView) inboxHolder.findViewById(R.id.nav_title)).setText("INBOX");

        //BCMPlans
        ((ImageView) bcmPlanHolder.findViewById(R.id.nav_icon)).setImageResource(R.drawable.ic_insert_drive_file_black_24dp);
        ((TextView) bcmPlanHolder.findViewById(R.id.nav_title)).setText("BCM PLANS");

        //Reporting
        ((ImageView) reportingHolder.findViewById(R.id.nav_icon)).setImageResource(R.drawable.ic_warning_black_24dp);
        ((TextView) reportingHolder.findViewById(R.id.nav_title)).setText("REPORTING");

        //More
        ((ImageView) moreHolder.findViewById(R.id.nav_icon)).setImageResource(R.drawable.ic_more_horiz_black_24dp);
        ((TextView) moreHolder.findViewById(R.id.nav_title)).setText("MORE");


        setBottomTabMenu(homeHolder);
        fm.beginTransaction().replace(R.id.container, new Home(), "Home").commit();
        toolbarTitle.setText("ALACER");

        Intent intent=getIntent();
        if(intent.getBooleanExtra("pending_notification",false)){
            fm.beginTransaction().replace(R.id.container, new Inbox(), "Inbox").commit();
            toolbarTitle.setText("INBOX");
            toolbarSearchIcon.setVisibility(View.GONE);
            setBottomTabMenu(findViewById(R.id.lay_inbox));
        }

    }

    public void openFragment(int id) {
        if (id == R.id.img_incident_reporting) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("is_superuser", false);
            Fragment fragInfo = new Reporting();
            fragInfo.setArguments(bundle);
            fm.beginTransaction().replace(R.id.container, fragInfo).commit();

            toolbarTitle.setText("REPORTING");
            toolbarSearchIcon.setVisibility(View.GONE);
            setBottomTabMenu(findViewById(R.id.lay_reporting));

        } else if (id == R.id.img_inbx_mascom) {
            fm.beginTransaction().replace(R.id.container, new Inbox(), "Inbox").commit();
            toolbarTitle.setText("INBOX");
            toolbarSearchIcon.setVisibility(View.GONE);
            setBottomTabMenu(findViewById(R.id.lay_inbox));
        } else if (id == R.id.img_bcm) {
            toolbarTitle.setText("BCM Plan");
            toolbarSearchIcon.setVisibility(View.VISIBLE);
            fm.beginTransaction().replace(R.id.container, new BCMPlan(UserMainActivity.this), "BCMPlANS").commit();
            setBottomTabMenu(findViewById(R.id.lay_bcm_plans));

        } else {
            fm.beginTransaction().replace(R.id.container, new Home(), "Home").commit();
            toolbarTitle.setText("ALACER");
            toolbarSearchIcon.setVisibility(View.GONE);
            setBottomTabMenu(findViewById(R.id.lay_home));
        }


    }

    private void setBottomTabMenu(View view) {
        if (previousSelectedBottomMenu != null) {
            previousSelectedBottomMenu.setBackgroundColor(getResources().getColor(R.color.nav_menu));
            ImageView homeIcon = (ImageView) previousSelectedBottomMenu.findViewById(R.id.nav_icon);
            homeIcon.setColorFilter(getResources().getColor(R.color.bottom_nav_text_color));
            TextView homeTitle = (TextView) previousSelectedBottomMenu.findViewById(R.id.nav_title);
            homeTitle.setTextColor(getResources().getColor(R.color.bottom_nav_text_color));
        }
        previousSelectedBottomMenu = view;

        view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        ImageView homeIcon = (ImageView) view.findViewById(R.id.nav_icon);
        homeIcon.setColorFilter(getResources().getColor(R.color.white));
        TextView homeTitle = (TextView) view.findViewById(R.id.nav_title);
        homeTitle.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lay_home:
                fm.beginTransaction().replace(R.id.container, new Home(), "Home").commit();
                toolbarTitle.setText("ALACER");
                setBottomTabMenu(view);
                toolbarSearchIcon.setVisibility(View.GONE);
                break;

            case R.id.lay_inbox:
                fm.beginTransaction().replace(R.id.container, new Inbox(), "Inbox").commit();
                toolbarTitle.setText("INBOX");
                toolbarSearchIcon.setVisibility(View.GONE);
                setBottomTabMenu(view);
                break;

            case R.id.lay_bcm_plans:
                toolbarTitle.setText("BCM Plan");
                toolbarSearchIcon.setVisibility(View.VISIBLE);
                fm.beginTransaction().replace(R.id.container, new BCMPlan(UserMainActivity.this), "BCMPlANS").commit();
                setBottomTabMenu(view);
                break;

            case R.id.lay_reporting:
                toolbarTitle.setText("REPORTING");
                fm.beginTransaction().replace(R.id.container, new Reporting(), "Reporting").commit();
                setBottomTabMenu(view);
                toolbarSearchIcon.setVisibility(View.GONE);
                break;

            case R.id.lay_more:
                toolbarTitle.setText("MORE");
                fm.beginTransaction().replace(R.id.container, new More(UserMainActivity.this), "More").commit();
                setBottomTabMenu(view);
                toolbarSearchIcon.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if(resultCode==RESULT_OK){
            if(requestCode==INBOXDETAIL){
                Fragment fragment=fm.findFragmentById(R.id.container);
                if(fragment instanceof Inbox){
                    ((Inbox)fragment).refreshInbox();
                }
            }

        }
    }
}
