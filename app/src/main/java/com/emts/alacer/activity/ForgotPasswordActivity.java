package com.emts.alacer.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 4/19/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_pass_toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Forgot Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final EditText etEmail = (EditText) findViewById(R.id.et_email);
        Button btnSend = (Button) findViewById(R.id.send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        forgetPasswordTask(etEmail.getText().toString().trim(), view);
                    } else {
                        AlertUtils.showSnack(ForgotPasswordActivity.this, view, "No Internet Connected !!!");
                    }
                } else {
                    AlertUtils.showSnack(ForgotPasswordActivity.this, view, "Please enter valid email address");
                }
            }
        });
    }

    private void forgetPasswordTask(String email, final View view) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, "Requesting ...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("email", email);

        vHelper.addVolleyRequestListeners(Api.getInstance().forgetPasswordUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            AlertUtils.showAlertMessage(ForgotPasswordActivity.this, "Message", resObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask ex", e.getMessage() + "");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showSnack(ForgotPasswordActivity.this, view, errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "forgetPasswordTask");

    }
}
