package com.emts.alacer.activity.superuser;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.AlacerApp;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.MultipartUtility;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Branch;
import com.emts.alacer.model.Department;
import com.emts.alacer.model.Report;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MassNotificationDetail extends AppCompatActivity {
    Report rList;
    CardView email, sms, mobile;
    CheckBox rEmail, rSms, rMobile;
    ImageView record, cancel, map;
    EditText message;
    Button send;
    TextView errorNotification, errorMessage, errorLocation, location, errorRecp, tvTimer, spinBranch,
            spinDept, deptList, branchList;
    String[] recipient = new String[]{"RECIPIENT", "EMPLOYEES", "BRANCH", "DEPARTMENT"};
    ArrayList<Department> departs;
    ArrayList<Branch> branches;
    Spinner spinRecp;
    int PLACE_PICKER_REQUEST = 1;
    String latitude = "3.1390029999999998";
    String longitude = "101.686855";
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(3.1390029999999998, 101.686855), new LatLng(3.1390029999999998, 101.686855));
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;
    boolean isClose = false;
    boolean isPlay = false;
    boolean isSave = false;
    Typeface typeface;
    CircularProgressBar circularProgressBar;
    public int seconds = 0;
    public int minutes = 0;
    //Declare the timer
    Timer timer;
    LinearLayout dept;
    String[] department;
    String[] branch;
    boolean[] checkedDepart, checkedBranch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass_notification_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(0);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Mass Notification");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        rList = (Report) intent.getSerializableExtra("report");
        departs = new ArrayList<>();
        branches = new ArrayList<>();
        ImageView icon = (ImageView) findViewById(R.id.logo_icon);
        TextView title = (TextView) findViewById(R.id.event_name);
        tvTimer = (TextView) findViewById(R.id.tv_timer);
        Picasso.with(getApplicationContext()).load(rList.getTypeImage()).into(icon);
        title.setText(rList.getTypeName());

        circularProgressBar = (CircularProgressBar) findViewById(R.id.cbProgress);

        email = (CardView) findViewById(R.id.lay_email);
        sms = (CardView) findViewById(R.id.lay_sms);
        mobile = (CardView) findViewById(R.id.lay_mobile);
        rEmail = (CheckBox) findViewById(R.id.rad_email);
        rSms = (CheckBox) findViewById(R.id.rad_sms);
        rMobile = (CheckBox) findViewById(R.id.rad_mob);
        deptList = (TextView) findViewById(R.id.tv_dept_list);
        branchList = (TextView) findViewById(R.id.tv_branch_list);

        typeface = Typeface.createFromAsset(getAssets(), "fonts/opensans_regular.ttf");

        random = new Random();
        record = (ImageView) findViewById(R.id.img_voice);
        cancel = (ImageView) findViewById(R.id.img_cancel);
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {
                audioTask();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPlay = false;
                isSave = false;
                isClose = false;
                AudioSavePathInDevice = "null";
                cancel.setVisibility(View.GONE);
                seconds = 0;
                minutes = 0;
                tvTimer.setText("0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                Picasso.with(MassNotificationDetail.this).load((R.drawable.btn_voice)).into(record);
            }
        });

        message = (EditText) findViewById(R.id.et_mesg);
        location = (TextView) findViewById(R.id.location);
        send = (Button) findViewById(R.id.send);

        errorNotification = (TextView) findViewById(R.id.validate_notification);
        errorMessage = (TextView) findViewById(R.id.validate_message);
        errorLocation = (TextView) findViewById(R.id.validate_location);
        errorRecp = (TextView) findViewById(R.id.validate_recp);
        map = (ImageView) findViewById(R.id.img_location);

        spinRecp = (Spinner) findViewById(R.id.spin_recp);
        spinBranch = (TextView) findViewById(R.id.spin_branch);
        spinDept = (TextView) findViewById(R.id.spin_department);
        final LinearLayout branch = (LinearLayout) findViewById(R.id.lay_spin_branch);
        dept = (LinearLayout) findViewById(R.id.lay_spin_dept);

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    startActivityForResult(intentBuilder.build(MassNotificationDetail.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
        if (NetworkUtils.isInNetwork(this)) {
            getDepartmentTask();
            getBranchTask();
        } else {
            AlertUtils.showSnack(this, toolbar, "No Internet Connected !!!");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MassNotificationDetail.this,
                R.layout.item_spinner_signup, recipient) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = LayoutInflater.from(MassNotificationDetail.this).inflate(R.layout.item_spinner_signup, parent, false);
                ((TextView) view).setTypeface(typeface);
                ((TextView) view).setText(recipient[position]);
                return view;
            }
        };
        spinRecp.setAdapter(adapter);
        spinRecp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinRecp.getSelectedItemPosition() == 2) {
                    Arrays.fill(checkedDepart, false); //set all depart unchecked
                    branch.setVisibility(View.VISIBLE);
                    dept.setVisibility(View.GONE);
                    deptList.setVisibility(View.GONE);
                    branchList.setVisibility(View.GONE);
                } else if (spinRecp.getSelectedItemPosition() == 3) {
                    Arrays.fill(checkedBranch, false);//set all branch unchecked
                    dept.setVisibility(View.VISIBLE);
                    branch.setVisibility(View.GONE);
                    deptList.setVisibility(View.GONE);
                    branchList.setVisibility(View.GONE);
                } else {
                    dept.setVisibility(View.GONE);
                    branch.setVisibility(View.GONE);
                    deptList.setVisibility(View.GONE);
                    branchList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rEmail.isChecked()) {
                    rEmail.setChecked(false);
                } else {
                    rEmail.setChecked(true);
                }
            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rSms.isChecked()) {
                    rSms.setChecked(false);
                } else {
                    rSms.setChecked(true);
                }
            }
        });

        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rMobile.isChecked()) {
                    rMobile.setChecked(false);
                } else {
                    rMobile.setChecked(true);
                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;
                if (rMobile.isChecked() || rSms.isChecked() || rEmail.isChecked()) {
                    errorNotification.setVisibility(View.GONE);
                } else {
                    isValid = false;
                    errorNotification.setText("Please choose atleast one of the notification method  ");
                    errorNotification.setVisibility(View.VISIBLE);

                }
                if (TextUtils.isEmpty(message.getText().toString().trim())) {
                    isValid = false;
                    errorMessage.setText("Please type your message");
                    errorMessage.setVisibility(View.VISIBLE);
                } else {
                    errorMessage.setVisibility(View.GONE);
                }
                if (spinRecp.getSelectedItemPosition() == 0) {
                    isValid = false;
                    errorRecp.setText("Please Choose valid recipient");
                    errorRecp.setVisibility(View.VISIBLE);
                } else {
                    if (spinRecp.getSelectedItemPosition() == 2) {
                        boolean isAnyBranchChecked = false;
                        //branch
                        for (int i = 0; i < checkedBranch.length; i++) {
                            if (checkedBranch[i]) {
                                isAnyBranchChecked = true;
                            }
                        }
                        if (!isAnyBranchChecked) {
                            isValid = false;
                            errorRecp.setText("Please select atleast one branch");
                            errorRecp.setVisibility(View.VISIBLE);
                        } else {
                            errorRecp.setVisibility(View.GONE);
                        }
                    } else if (spinRecp.getSelectedItemPosition() == 3) {
                        //department
                        boolean isAnyDepartChecked = false;
                        //branch
                        for (int i = 0; i < checkedDepart.length; i++) {
                            if (checkedDepart[i]) {
                                isAnyDepartChecked = true;
                            }
                        }
                        if (!isAnyDepartChecked) {
                            isValid = false;
                            errorRecp.setText("Please select atleast one department");
                            errorRecp.setVisibility(View.VISIBLE);
                        } else {
                            errorRecp.setVisibility(View.GONE);
                        }
                    } else {
                        errorRecp.setVisibility(View.GONE);
                    }
                }
                if (location.getText().toString().trim().equals("LOCATION")) {
                    isValid = false;
                    errorLocation.setText("Please choose the location");
                    errorLocation.setVisibility(View.VISIBLE);
                } else {
                    errorLocation.setVisibility(View.GONE);
                }
                if (isValid) {
                    if (NetworkUtils.isInNetwork(MassNotificationDetail.this)) {
                        String token = String.valueOf(PreferenceHelper.getInstance(AlacerApp.getInstance().getApplicationContext()).getToken());
                        String apiKey = Api.getInstance().apiKey;
                        String messages = message.getText().toString().trim();
                        String locations = location.getText().toString().trim();
                        String lat = latitude;
                        String longs = longitude;

//                        new SendMassNotific ation().execute(Api.getInstance().massNotificationUrl, token, AudioSavePathInDevice,
//                                apiKey, message.getText().toString().trim(), location.getText().toString().trim(),
//                                latitude, longitude, spinRecp.getSelectedItem().toString(),
//                                departs.get(spinDept.getSelectedItemPosition()).getDepartId(),
//                                branches.get(spinBranch.getSelectedItemPosition()).getBranchId(), rList.getId());

                        sendMassNotificationTask(messages, locations, lat, longs);
                    } else {
                        AlertUtils.showSnack(getApplicationContext(), toolbar, "No Internet Connected!!");
                    }
                } else {
                    AlertUtils.showSnack(getApplicationContext(), toolbar, "Please validate your form");
                }


            }
        });

//        String url = "https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap\n" +
//                "&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318\n" +
//                "&markers=color:red%7Clabel:C%7C40.718217,-73.998284\n" +
//                "&key=AIzaSyDkvT0UMpnezvmnTdn2W2lUl-YZ5VYfgi4";

        setMapImage(latitude, longitude);
    }

    private void audioTask() {
        if (checkPermission()) {
            if (isPlay) {
                if (isClose) {
                    if (mediaPlayer != null) {
                        mediaPlayer.stop();
                        Picasso.with(MassNotificationDetail.this).load((R.drawable.btn_play)).into(record);
                        mediaPlayer.release();
                        MediaRecorderReady();
                        Toast.makeText(MassNotificationDetail.this, "Audio stopped",
                                Toast.LENGTH_LONG).show();
                        isClose = false;
                        cancel.setVisibility(View.VISIBLE);
                    }
                } else {
                    isClose = true;
                    Picasso.with(MassNotificationDetail.this).load((R.drawable.btn_hold)).into(record);
                    cancel.setVisibility(View.GONE);
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(AudioSavePathInDevice);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        Log.e("record exception", e.getMessage());
                    }
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            totalLength = mediaPlayer.getDuration();
                            mHandler.post(runnable);
                            circularProgressBar.setVisibility(View.VISIBLE);
                            circularProgressBar.setProgress(0);
                        }
                    });
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            mediaPlayer.stop();
                            Picasso.with(MassNotificationDetail.this).load((R.drawable.btn_play)).into(record);
                            mediaPlayer.release();
                            MediaRecorderReady();
                            isClose = false;
                            cancel.setVisibility(View.VISIBLE);
                        }
                    });
                    Toast.makeText(MassNotificationDetail.this, "Recording Playing",
                            Toast.LENGTH_LONG).show();
                }

            } else if (isSave) {
                saveRecord();
            } else {
                MediaRecorderReady();
                showTimer();
                Picasso.with(MassNotificationDetail.this).load((R.drawable.btn_recording)).into(record);
                File storagePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Alacer");
                storagePath.mkdirs();
                AudioSavePathInDevice = storagePath.getPath() + "/" + CreateRandomAudioFileName(5) + ".3gp";
//                        AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Alacer" + "/" + CreateRandomAudioFileName(5) + ".3gp";
                mediaRecorder.setOutputFile(AudioSavePathInDevice);

                try {
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                } catch (IllegalStateException e) {
                    Logger.e("recordStateEx:", e.getMessage());
                } catch (IOException e) {
                    Logger.e("recordIOEx:", e.getMessage());
                }

//                cancel.setVisibility(View.VISIBLE);
                isSave = true;
                Toast.makeText(MassNotificationDetail.this, "Recording started",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            requestPermission();
        }
    }

    int totalLength = 0;
    int mProgress = 0;
    Handler mHandler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            circularProgressBar.setVisibility(View.VISIBLE);
            try {
                if (totalLength > 0 && mediaPlayer.isPlaying()) {
                    mProgress = (int) (((float) mediaPlayer.getCurrentPosition() / (float) totalLength) * 100);
                    Logger.e("mProgress calculate", mediaPlayer.getDuration() + " Current:" + mediaPlayer.getCurrentPosition());
                    mHandler.postDelayed(runnable, 1000);
                    Logger.e("service progres", mProgress + " ");
                    circularProgressBar.setProgress(mProgress);
                }
                if (mProgress <= 0 || mProgress >= 100) {
                    circularProgressBar.setVisibility(View.GONE);
                    circularProgressBar.setProgress(0);
                }
                if (!mediaPlayer.isPlaying()) {
                    mHandler.removeCallbacks(runnable);
                }
            } catch (IllegalStateException e) {
                circularProgressBar.setVisibility(View.GONE);
                circularProgressBar.setProgress(0);
                mHandler.removeCallbacks(runnable);
            }
        }
    };

    private void saveRecord() {
        mediaRecorder.stop();
        stopTimer();
        isPlay = true;
        cancel.setVisibility(View.VISIBLE);
        Picasso.with(MassNotificationDetail.this).load((R.drawable.btn_play)).into(record);
        Toast.makeText(MassNotificationDetail.this, "Recording Completed",
                Toast.LENGTH_LONG).show();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MassNotificationDetail.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        audioTask();
                    } else {
                        Toast.makeText(MassNotificationDetail.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    private String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }

    private void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void showTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        tvTimer.setVisibility(View.VISIBLE);
                        if (seconds < 10 & minutes < 10) {
                            tvTimer.setText("0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                        } else if (minutes < 10) {
                            tvTimer.setText("0" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        } else if (seconds < 10) {
                            tvTimer.setText(String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                        } else {
                            tvTimer.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        }
                        seconds += 1;

                        if (seconds == 60) {
                            if (minutes < 10) {
                                tvTimer.setText("0" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                            } else {
                                tvTimer.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                            }
                            seconds = 00;
                            minutes = minutes + 1;

                        }


                    }

                });
            }

        }, 0, 1000);
    }

    public void stopTimer() {
        try {
            timer.cancel();
            tvTimer.setVisibility(View.GONE);
        } catch (Exception e) {

        }

    }

    private void sendMassNotificationTask(String message, String location, String latitude, String longitude) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(MassNotificationDetail.this, "Sending...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("type_id", rList.getId());
        postParam.put("token", PreferenceHelper.getInstance(MassNotificationDetail.this).getToken());
        postParam.put("location", location);
        postParam.put("message", message);
        postParam.put("longitude", longitude);
        postParam.put("incident_id", "0");
        postParam.put("latitude", latitude);
        postParam.put("recipient", spinRecp.getSelectedItem().toString().toLowerCase());
        if (spinRecp.getSelectedItemPosition() == 3) {
            int index = 0;
            for (int i = 0; i < checkedDepart.length; i++) {
                if (checkedDepart[i]) {
                    postParam.put("department_ids[" + index + "]", departs.get(i).getDepartId());
                    index++;
                }
            }
        }
//        else {
//            postParam.put("department_ids[0]", "");
//        }

        if (spinRecp.getSelectedItemPosition() == 2) {
            int index = 0;
            for (int i = 0; i < checkedBranch.length; i++) {
                if (checkedBranch[i]) {
                    postParam.put("branch_ids[" + index + "]", branches.get(i).getBranchId());
                    index++;
                }
            }
        }
//        else {
//            postParam.put("branch_ids[0]", "");
//        }

        if (rSms.isChecked()) {
            postParam.put("sms", "sms");
        } else {
            postParam.put("sms", "");
        }
        if (rEmail.isChecked()) {
            postParam.put("email", "email");
        } else {
            postParam.put("email", "");
        }
        if (rMobile.isChecked()) {
            postParam.put("mobile_notification", "mobile");
        } else {
            postParam.put("mobile_notification", "");
        }

        HashMap<String, String> dataPost = new HashMap<>();
        if (!TextUtils.isEmpty(AudioSavePathInDevice)) {
            dataPost.put("audio", AudioSavePathInDevice);
            Logger.e("audiopath", AudioSavePathInDevice);
        }
        vHelper.addMultipartRequest(Api.getInstance().massNotificationUrl, Request.Method.POST, postParam, dataPost,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {

                        try {
                            JSONObject reObj = new JSONObject(response);
                            if (reObj.getBoolean("status")) {
//                                AlertUtils.showAlertMessage(MassNotificationDetail.this, "Message", reObj.getString("message"));
                                AlertUtils.showToast(getApplicationContext(), reObj.getString("message"));
                                onBackPressed();
                            } else {
                                AlertUtils.showAlertMessage(MassNotificationDetail.this, "Message", reObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(MassNotificationDetail.this, errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "sendMassNotificationTask");
    }


    class SendMassNotification extends AsyncTask<String, Integer, String> {

        final ProgressDialog pDialog = AlertUtils.showProgressDialog(MassNotificationDetail.this, "Sending...");

        @Override
        protected void onPreExecute() {

            pDialog.show();

            //todo show the update progress bar
            // uploadProgressLayout.setVisibility(View.GONE);
            // imageUploadProgressBar.setProgress(0);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String res = "";
            String audioPath = "";

            String url = strings[0];
            Logger.e("doInBackground UploadFileToServer url", url + "");
            String token = strings[1];
            Logger.e("doInBackground UploadFileToServer token", token + "");
            if (!TextUtils.isEmpty(strings[2])) {
                audioPath = strings[2];
            }
            Logger.e("doInBackground UploadFileToServer path", audioPath + "");
            String apiKey = strings[3];
            Logger.e("doInBackground apiKey", apiKey + "");
            String message = strings[4];
            Logger.e("doInBackground message", message + "");
            String location = strings[5];
            Logger.e("doInBackground location", location + "");
            String latitude = strings[6];
            Logger.e("doInBackground latitude", latitude + "");
            String longitude = strings[7];
            Logger.e("doInBackground longitude", longitude + "");
            String recipient = strings[8];
            Logger.e("doInBackground recipient", recipient + "");
            String department_id = strings[9];
            Logger.e("doInBackground department_id", department_id + "");
            String branch_id = strings[10];
            Logger.e("doInBackground branch_id", branch_id + "");
            String type_id = strings[11];
            Logger.e("doInBackground type_id", type_id + "");

            try {
                MultipartUtility multipartUtility = new MultipartUtility(url, "UTF-8", token);
                if (!TextUtils.isEmpty(audioPath)) {
                    multipartUtility.addFilePart("audio", new File(audioPath));
                }
                multipartUtility.addFormField("api_key", apiKey);
                multipartUtility.addFormField("token", token);
//                multipartUtility.addFormField("incident_id", rList.getId());
                multipartUtility.addFormField("type_id", type_id);
                multipartUtility.addFormField("location", location);
                multipartUtility.addFormField("message", message);
                multipartUtility.addFormField("latitude", latitude);
                multipartUtility.addFormField("longitude", longitude);
                multipartUtility.addFormField("recipient", recipient.toLowerCase());
                if (spinRecp.getSelectedItemPosition() == 3) {
                    multipartUtility.addFormField("department_id", department_id);
                } else {
                    multipartUtility.addFormField("department_id", "");
                }
                if (spinRecp.getSelectedItemPosition() == 2) {
                    multipartUtility.addFormField("branch_id", branch_id);
                } else {
                    multipartUtility.addFormField("branch_id", "");
                }
                if (rSms.isChecked()) {
                    multipartUtility.addFormField("sms", "sms");
                } else {
                    multipartUtility.addFormField("sms", "");
                }
                if (rEmail.isChecked()) {
                    multipartUtility.addFormField("email", "email");
                } else {
                    multipartUtility.addFormField("email", "");
                }
                if (rMobile.isChecked()) {
                    multipartUtility.addFormField("mobile_notification", "mobile");
                } else {
                    multipartUtility.addFormField("mobile_notification", "");
                }
                //add dummy field for /n problem
                multipartUtility.addFormField("dummy", "");

                if (!TextUtils.isEmpty(audioPath)) {
                    File file = new File(audioPath);
                    Logger.e("check File", file.exists() + "ok");
                }

                List<String> response = multipartUtility.finish();

                Logger.e("SERVER REPLIED:", "replied");

                for (String line : response) {
                    res = res + line;
                    Logger.e("response as list", line);
                }

            } catch (IOException e) {
                Logger.e("exception by multipart", e.getMessage() + "");
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            Logger.e("UploadFileToServer", "Response from server: " + result);

            try {
                JSONObject reObj = new JSONObject(result);
                if (reObj.getBoolean("status")) {
                    AlertUtils.showAlertMessage(MassNotificationDetail.this, "Message", reObj.getString("message"));
                } else {
                    AlertUtils.showAlertMessage(MassNotificationDetail.this, "Message", reObj.getString("message"));

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
            }
            pDialog.dismiss();

            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            //  imageUploadProgressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            //  imageUploadProgressBar.setProgress(progress[0]);

            // updating percentage value
            //  imageUploadProgress.setText(String.valueOf(progress[0]) + "%");
//            if (progress[0] == 100) {
//                cancelImageUpload.setVisibility(View.GONE);
//            } else {
//                cancelImageUpload.setVisibility(View.VISIBLE);
//            }
            Logger.e("onProgressUpdate img upload", progress[0] + "");
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                location.setText(place.getName());
                Double lat = place.getLatLng().latitude;
                Double longs = place.getLatLng().longitude;

                latitude = lat.toString().trim();
                longitude = longs.toString().trim();

                setMapImage(latitude, longitude);
            }
        }
    }

    private void setMapImage(String latitude, String longitude) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude
                + "&zoom=15&size=600x300&markers=color:blue%7Clabel:S%7C11211%7C11206%7C11222&key=" + getString(R.string.google_api_key);
        Picasso.with(getApplicationContext()).load(url).into(map);
    }

    private void getDepartmentTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getDepartmentUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray departArray = resObj.getJSONArray("department_list");
                                for (int i = 0; i < departArray.length(); i++) {
                                    JSONObject eachObj = departArray.getJSONObject(i);
                                    Department depart = new Department();
                                    depart.setDepartName(eachObj.getString("name"));
                                    depart.setDepartId(eachObj.getString("id"));

                                    departs.add(depart);
                                }

                                department = new String[departs.size()];
                                checkedDepart = new boolean[departs.size()];
//                                spinDept.setAdapter(adapter);
                                spinDept.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showListAlert(departs, department, checkedDepart);
                                    }
                                });
                            }
//                            departLoad = true;
//                            checkProgress();
                        } catch (JSONException e) {
                            Logger.e("getDepart res ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
//                        departLoad = true;
//                        checkProgress();
                    }
                }, "getDepartmentTask");
    }

    private void showListAlert(final ArrayList<Department> departs, String[] colors, final boolean[] checkedColors) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MassNotificationDetail.this);

        // String array for alert dialog multi choice items
        for (int i = 0; i < departs.size(); i++) {
            colors[i] = departs.get(i).getDepartName();

            // Boolean array for initial selected items
            if (checkedColors[i]) {
                checkedColors[i] = true;
            } else {
                checkedColors[i] = false;
            }
        }

        // Convert the color array to list
        final List<String> colorsList = Arrays.asList(colors);

        // Set multiple choice items for alert dialog
                /*
                    AlertDialog.Builder setMultiChoiceItems(CharSequence[] items, boolean[]
                    checkedItems, DialogInterface.OnMultiChoiceClickListener listener)
                        Set a list of items to be displayed in the dialog as the content,
                        you will be notified of the selected item via the supplied listener.
                 */
                /*
                    DialogInterface.OnMultiChoiceClickListener
                    public abstract void onClick (DialogInterface dialog, int which, boolean isChecked)

                        This method will be invoked when an item in the dialog is clicked.

                        Parameters
                        dialog The dialog where the selection was made.
                        which The position of the item in the list that was clicked.
                        isChecked True if the click checked the item, else false.
                 */
        builder.setMultiChoiceItems(colors, checkedColors, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedColors[which] = isChecked;

                // Get the current focused item
                String currentItem = colorsList.get(which);

                // Notify the current action
//                Toast.makeText(getApplicationContext(),
//                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Select Department");

        // Set the positive/yes button click listener
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                deptList.setText(" ");
                String departName = "";
                for (int i = 0; i < checkedColors.length; i++) {
                    boolean checked = checkedColors[i];
                    if (checked) {
                        checkedColors[i] = true;
                        if (departName.equals("")) {
                            departName = departs.get(i).getDepartName();
                        } else {
                            departName = departName + "\n" + departs.get(i).getDepartName();
                        }
                    }
                }
                if (!TextUtils.isEmpty(departName.trim())) {
                    deptList.setText(departName.trim());
                    deptList.setVisibility(View.VISIBLE);
                } else {
                    deptList.setVisibility(View.GONE);
                }
                Logger.e("depart_nme", departName + " ");

            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();

    }

    private void getBranchTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getBranchUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray departArray = resObj.getJSONArray("branch_list");
                                for (int i = 0; i < departArray.length(); i++) {
                                    JSONObject eachObj = departArray.getJSONObject(i);
                                    Branch branch = new Branch();
                                    branch.setBranchName(eachObj.getString("name"));
                                    branch.setBranchId(eachObj.getString("id"));

                                    branches.add(branch);
                                }
                                branch = new String[branches.size()];
                                checkedBranch = new boolean[branches.size()];
                                spinBranch.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showBranchListAlert(branches, branch, checkedBranch);
                                    }
                                });


//                                spinBranch.setAdapter(adapter);
                            }
//                            branchLoad = true;
//                            checkProgress();
                        } catch (JSONException e) {
                            Logger.e("getBranch res ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
//                        branchLoad = true;
//                        checkProgress();
                    }
                }, "getBranchTask");
    }

    private void showBranchListAlert(final ArrayList<Branch> branches, String[] colors, final boolean[] checkedColors) {

        AlertDialog.Builder builder = new AlertDialog.Builder(MassNotificationDetail.this);

        // String array for alert dialog multi choice items
        for (int i = 0; i < branches.size(); i++) {
            colors[i] = branches.get(i).getBranchName();

            // Boolean array for initial selected items
            if (checkedColors[i]) {
                checkedColors[i] = true;
            } else {
                checkedColors[i] = false;
            }
        }

        // Convert the color array to list
        final List<String> colorsList = Arrays.asList(colors);

        // Set multiple choice items for alert dialog
                /*
                    AlertDialog.Builder setMultiChoiceItems(CharSequence[] items, boolean[]
                    checkedItems, DialogInterface.OnMultiChoiceClickListener listener)
                        Set a list of items to be displayed in the dialog as the content,
                        you will be notified of the selected item via the supplied listener.
                 */
                /*
                    DialogInterface.OnMultiChoiceClickListener
                    public abstract void onClick (DialogInterface dialog, int which, boolean isChecked)

                        This method will be invoked when an item in the dialog is clicked.

                        Parameters
                        dialog The dialog where the selection was made.
                        which The position of the item in the list that was clicked.
                        isChecked True if the click checked the item, else false.
                 */
        builder.setMultiChoiceItems(colors, checkedColors, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedColors[which] = isChecked;

                // Get the current focused item
                String currentItem = colorsList.get(which);

                // Notify the current action
//                Toast.makeText(getApplicationContext(),
//                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Select Branch");

        // Set the positive/yes button click listener
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                branchList.setText(" ");
                String departName = "";
                for (int i = 0; i < checkedColors.length; i++) {
                    boolean checked = checkedColors[i];
                    if (checked) {
                        checkedColors[i] = true;
                        if (departName.equals("")) {
                            departName = branches.get(i).getBranchName();
                        } else {
                            departName = departName + "\n" + branches.get(i).getBranchName();
                        }
                    }
                }
                if (!TextUtils.isEmpty(departName.trim())) {
                    branchList.setText(departName);
                    branchList.setVisibility(View.VISIBLE);
                } else {
                    branchList.setVisibility(View.GONE);
                }
                Logger.e("branch list _nme", departName + " ");

            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }
}
