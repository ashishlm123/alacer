package com.emts.alacer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.model.Report;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;

/**
 * Created by Srijana on 4/25/2017.
 */

public class BCMDetailActivity extends AppCompatActivity implements DownloadFile.Listener {
    WebView detail;
    Report bcm;
    ProgressDialog pDialog;
    Toolbar toolbar;
    TextView toolbarTitle;
    //    PDFView pdfView;
//    Integer pageNumber = 0;
//    String pdfFileName;
    RemotePDFViewPager remotePDFViewPager;
    PDFPagerAdapter adapter;

    ProgressBar progressBar;
    LinearLayout pdfReaderContainer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bcm_detail);


        Intent intent = getIntent();
        if (intent != null) {
            bcm = (Report) intent.getSerializableExtra("bcm");
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(bcm.getTypeName());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progress);
        pdfReaderContainer = (LinearLayout) findViewById(R.id.pdf_reader_container);

//        pdfView = (PDFView) findViewById(R.id.bcm_details);
//        displayFromUrl(bcm.getTypeImage());

        initWebViewPdfReader();

//        remotePDFViewPager = new RemotePDFViewPager(getApplicationContext(), bcm.getTypeImage(), this);
//        remotePDFViewPager.setId(R.id.pdfViewPager);

    }

//    private void displayFromUrl(String typeImage) {
//
//        pdfView.fromUri(Uri.parse(typeImage))
//                .defaultPage(pageNumber)
//                .enableSwipe(true)
//                .swipeHorizontal(false)
//                .onPageChange(this)
//                .enableAnnotationRendering(true)
//                .onLoad(this)
//                .scrollHandle(new DefaultScrollHandle(this))
//                .load();
//    }
//
//
//    @Override
//    public void loadComplete(int nbPages) {
//        PdfDocument.Meta meta = pdfView.getDocumentMeta();
//        printBookmarksTree(pdfView.getTableOfContents(), "-");
//
//    }
//    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
//        for (PdfDocument.Bookmark b : tree) {
//
//            Log.e("pdf", String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
//
//            if (b.hasChildren()) {
//                printBookmarksTree(b.getChildren(), sep + "-");
//            }
//        }
//    }
//
//    @Override
//    public void onPageChanged(int page, int pageCount) { pageNumber = page;
//        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
//    }


    private void initWebViewPdfReader() {
        detail = (WebView) findViewById(R.id.bcm_detail);
        detail.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();
            }
        });
        detail.getSettings().setJavaScriptEnabled(true);
        detail.getSettings().setAllowFileAccess(true);
        detail.getSettings().setPluginState(WebSettings.PluginState.ON);
        detail.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                Logger.e("newProgress", "Progress:" + newProgress);
                super.onProgressChanged(view, newProgress);
            }
        });

        pDialog = new ProgressDialog(BCMDetailActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

//        String doc = "<iframe src='http://docs.google.com/viewer?url=" + bcm.getTypeImage() + "' width='100%' height='100%' style='border: none;'></iframe>";
        String doc = "http://docs.google.com/viewer?url=" + bcm.getTypeImage();
        detail.loadUrl(doc);

        Logger.e("url", doc);
    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        Logger.e("onSuccess", url + "\nDest: " + destinationPath);
        adapter = new PDFPagerAdapter(this, destinationPath);
        remotePDFViewPager.setAdapter(adapter);
//        setContentView(remotePDFViewPager);
        pdfReaderContainer.addView(remotePDFViewPager);

    }

    @Override
    public void onFailure(Exception e) {
        Logger.e("onFailure", e.getMessage() + " **");
    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        Logger.e("onProgressUpdate", progress + " TOtal: " + total);
        progressBar.setProgress(progress / total * 100);
    }
}
