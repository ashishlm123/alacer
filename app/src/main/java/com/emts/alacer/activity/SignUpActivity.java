package com.emts.alacer.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Branch;
import com.emts.alacer.model.Department;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 4/19/2017.
 */

public class SignUpActivity extends AppCompatActivity {
    EditText etMobile, etEmail, etPassword, etName;
    CheckBox termNCond;
    LinearLayout holderTNC;
    TextView errorMobile, errorEmail, errorPassword, errorTNC, errorDept, errorBranch, errorName;
    Button btnSignUp;
    Spinner spinDept, spinBranch;
    ProgressDialog pDialog;
    boolean departLoad, branchLoad;
    ArrayList<Department> departs;
    ArrayList<Branch> branches;
    PreferenceHelper prefHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        prefHelper = PreferenceHelper.getInstance(this);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.signup_toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Sign Up");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etMobile = (EditText) findViewById(R.id.et_mobile);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        etPassword.setTypeface(etEmail.getTypeface());
        etName = (EditText) findViewById(R.id.et_name);
        termNCond = (CheckBox) findViewById(R.id.check_tnc);
        holderTNC = (LinearLayout) findViewById(R.id.lay_tnc);
        errorEmail = (TextView) findViewById(R.id.error_email);
        errorMobile = (TextView) findViewById(R.id.error_mobile);
        errorPassword = (TextView) findViewById(R.id.error_password);
        errorName = (TextView) findViewById(R.id.error_name);
        errorTNC = (TextView) findViewById(R.id.error_tnc);
        btnSignUp = (Button) findViewById(R.id.btn_signup);
        spinBranch = (Spinner) findViewById(R.id.spin_branch);
        spinDept = (Spinner) findViewById(R.id.spin_dept);
        errorDept = (TextView) findViewById(R.id.error_dept);
        errorBranch = (TextView) findViewById(R.id.error_branch);

        holderTNC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (termNCond.isChecked()) {
                    termNCond.setChecked(false);
                } else {
                    termNCond.setChecked(true);
                }
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetworkUtils.isInNetwork(SignUpActivity.this)) {
                    AlertUtils.showSnack(SignUpActivity.this, view, "No Internet Connected !!!");
                    return;
                }
                boolean isValid = true;

                if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
                    isValid = false;
                    errorMobile.setText("This field is required");
                    errorMobile.setVisibility(View.VISIBLE);
                } else {
                    errorMobile.setVisibility(View.GONE);
                }
                if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                    isValid = false;
                    errorName.setText("This field is required");
                    errorName.setVisibility(View.VISIBLE);
                } else {
                    errorName.setVisibility(View.GONE);
                }

                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    isValid = false;
                    errorEmail.setText("This field is required");
                    errorEmail.setVisibility(View.VISIBLE);
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                        isValid = false;
                        errorEmail.setText("Please enter valid email address");
                        errorEmail.setVisibility(View.VISIBLE);
                    } else {
                        errorEmail.setVisibility(View.GONE);
                    }
                }


                if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    isValid = false;
                    errorPassword.setText("This field is required");
                    errorPassword.setVisibility(View.VISIBLE);
                } else {
                    errorPassword.setVisibility(View.GONE);
                }

                if (!termNCond.isChecked()) {
                    isValid = false;
                    errorTNC.setText("Please agree the terms and conditions");
                    errorTNC.setVisibility(View.VISIBLE);
                } else {
                    errorTNC.setVisibility(View.GONE);
                }
                if (spinDept.getSelectedItemPosition() == 0) {
                    isValid = false;
                    errorDept.setText("Please select department");
                    errorDept.setVisibility(View.VISIBLE);
                } else {
                    errorDept.setVisibility(View.GONE);
                }
                if (spinBranch.getSelectedItemPosition() == 0) {
                    isValid = false;
                    errorBranch.setText("Please select branch");
                    errorBranch.setVisibility(View.VISIBLE);
                } else {
                    errorBranch.setVisibility(View.GONE);
                }

                if (isValid) {
                    registrationTask(etMobile.getText().toString().trim(),
                            etEmail.getText().toString().trim(),
                            etPassword.getText().toString().trim(),
                            etName.getText().toString().trim(),
                            ((Department) spinDept.getSelectedItem()).getDepartId(),
                            ((Branch) spinBranch.getSelectedItem()).getBranchId());
                } else {
                    AlertUtils.showSnack(getApplicationContext(), toolbar, "Please validate the information filled in form");
                }
            }
        });

        departs = new ArrayList<>();
        branches = new ArrayList<>();

        if (NetworkUtils.isInNetwork(this)) {
            pDialog = AlertUtils.showProgressDialog(this, "Please wait...");
            pDialog.show();
            getDepartmentTask();
            getBranchTask();
        } else {
            AlertUtils.showSnack(this, toolbar, "No Internet Connected !!!");
        }

    }

    private void registrationTask(String mobile, String email, final String password, final String name,
                                  String departId, String branchId) {
        pDialog = AlertUtils.showProgressDialog(SignUpActivity.this, "Registering ...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("mobile_no", mobile);
        postParams.put("email", email);
        postParams.put("password", password);
        postParams.put("name", name);
        postParams.put("department_id", departId);
        postParams.put("branch_id", branchId);
        postParams.put("t_c", "yes");
        String pushId = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushId)) {
            postParams.put("push_id", pushId);
        }
//        postParams.put("token", "lkasdfjksldfkjsldaf");

        vHelper.addVolleyRequestListeners(Api.getInstance().registerUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                if (resObj.getString("login").equalsIgnoreCase("yes")) {
                                    JSONObject userObj = resObj.getJSONObject("user_detail");
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_ID, userObj.getString("id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("name")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_MOBILE, userObj.getString("mobile_no")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_DEPART_ID, userObj.getString("department_id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.APP_USER_BRANCH_ID, userObj.getString("branch_id")).commit();
                                    prefHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                                    if (userObj.getString("user_type").equals("1")) {
                                        prefHelper.edit().putString(PreferenceHelper.APP_USER_TYPE, PreferenceHelper.APP_USER_SUPER_USER).commit();
                                    } else {
                                        prefHelper.edit().putString(PreferenceHelper.APP_USER_TYPE, PreferenceHelper.APP_USER_USER).commit();
                                    }
                                    prefHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();

                                    Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                } else {
                                    AlertUtils.showAlertMessage(SignUpActivity.this, "Register Success !!!", resObj.getString("message"));
                                }
                            } else {
                                AlertUtils.showAlertMessage(SignUpActivity.this, "Register Error !!!", resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("registrationTask ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showAlertMessage(SignUpActivity.this, "Register Error !!!", errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("registrationTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "registrationTask");

    }

    private void getDepartmentTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getDepartmentUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray departArray = resObj.getJSONArray("department_list");

                                Department depart1 = new Department();
                                depart1.setDepartName("Department");
                                depart1.setDepartId("0");
                                departs.add(depart1);
                                for (int i = 0; i < departArray.length(); i++) {
                                    JSONObject eachObj = departArray.getJSONObject(i);
                                    Department depart = new Department();
                                    depart.setDepartName(eachObj.getString("name"));
                                    depart.setDepartId(eachObj.getString("id"));
                                    departs.add(depart);
                                }

                                ArrayAdapter<Department> adapter = new ArrayAdapter<>(SignUpActivity.this,
                                        R.layout.item_spinner_signup, departs);
                                spinDept.setAdapter(adapter);
                            }
                            departLoad = true;
                            checkProgress();
                        } catch (JSONException e) {
                            Logger.e("getDepart res ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        departLoad = true;
                        checkProgress();
                    }
                }, "getDepartmentTask");
    }

    private void getBranchTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getBranchUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray departArray = resObj.getJSONArray("branch_list");

                                Branch branch1 = new Branch();
                                branch1.setBranchName("Branch");
                                branch1.setBranchId("0");
                                branches.add(branch1);
                                for (int i = 0; i < departArray.length(); i++) {
                                    JSONObject eachObj = departArray.getJSONObject(i);
                                    Branch branch = new Branch();
                                    branch.setBranchName(eachObj.getString("name"));
                                    branch.setBranchId(eachObj.getString("id"));
                                    branches.add(branch);
                                }

                                ArrayAdapter<Branch> adapter = new ArrayAdapter<>(SignUpActivity.this,
                                        R.layout.item_spinner_signup, branches);
                                spinBranch.setAdapter(adapter);
                            }
                            branchLoad = true;
                            checkProgress();
                        } catch (JSONException e) {
                            Logger.e("getBranch res ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        branchLoad = true;
                        checkProgress();
                    }
                }, "getBranchTask");
    }

    private void checkProgress() {
        if (branchLoad && departLoad && pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
