package com.emts.alacer.activity;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.PushNotificationHelper;
import com.google.firebase.iid.FirebaseInstanceId;

public class UserSelectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_select);

        ImageView imgTest = (ImageView) findViewById(R.id.img_test);
        imgTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PushNotificationHelper.sendNotification(FirebaseInstanceId.getInstance().getToken());
            }
        });

        Button btnUser = (Button) findViewById(R.id.btn_user);
        Button btnSuperUser = (Button) findViewById(R.id.btn_super_user);

        btnUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("user", true);
                startActivity(intent);
            }
        });

        btnSuperUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("user", false);
                startActivity(intent);
            }
        });
    }
}
