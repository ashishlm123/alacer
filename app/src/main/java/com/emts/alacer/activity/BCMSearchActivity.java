package com.emts.alacer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.BCMMenuAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Report;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 4/26/2017.
 */

public class BCMSearchActivity extends AppCompatActivity {

    ImageView search, cross;
    EditText text;
    RecyclerView bcmList;
    ArrayList<Report> bList;
    ArrayList<Report> aftersearlist;
    BCMMenuAdapter adapter;
    ProgressBar progressBar;
    TextView errorText,cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bcm_search);

        search = (ImageView) findViewById(R.id.search);
        cancel = (TextView) findViewById(R.id.custom_toolbar_title);
        text = (EditText) findViewById(R.id.et_text);
        bcmList = (RecyclerView) findViewById(R.id.bcm_menu);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        errorText = (TextView) findViewById(R.id.error_mesg);
        aftersearlist = new ArrayList<>();
        cross=(ImageView)findViewById(R.id.cancel);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setText("");
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            bList = (ArrayList<Report>) intent.getSerializableExtra("bcm");
            aftersearlist.addAll(bList);
            bcmList.setVisibility(View.VISIBLE);
        }
        if (bList.size() < 1) {
            if (NetworkUtils.isInNetwork(this)) {
                bcmPlanListingTask();
                bcmList.setVisibility(View.GONE);
            } else {
                errorText.setText("No Internet Connected !!");
                errorText.setVisibility(View.VISIBLE);
                bcmList.setVisibility(View.GONE);
            }
        }


        adapter = new BCMMenuAdapter(aftersearlist, BCMSearchActivity.this);
        bcmList.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BCMSearchActivity.this);
        bcmList.setLayoutManager(linearLayoutManager);


        // Add Text Change Listener to EditText
        text.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                aftersearlist.clear();
                if (s.length() > 0) {
                    for (int i = 0; i < bList.size(); i++) {
                        if ((bList.get(i).getTypeName().toLowerCase()).contains(s.toString().toLowerCase())) {
                            aftersearlist.add(bList.get(i));
                        }
                    }
                } else {
                    aftersearlist.addAll(bList);
                }
                adapter.notifyDataSetChanged();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void bcmPlanListingTask() {

        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().bcmPlan, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);

                            if (resObj.getBoolean("status")) {
                                JSONArray array = resObj.getJSONArray("bcm_plans");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject eachObj = array.getJSONObject(i);
                                    Report report = new Report();
                                    report.setTypeName(eachObj.getString("name"));
                                    if (!TextUtils.isEmpty(eachObj.getString("file_saved")))
                                        report.setTypeImage(resObj.getString("file_path") + eachObj.getString("file_saved"));
                                    bList.add(report);
                                    aftersearlist.add(report);
                                }

                                adapter.notifyDataSetChanged();
                                bcmList.setVisibility(View.VISIBLE);
                            } else {
                                AlertUtils.showToast(getApplicationContext(), resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("bcmPlanListingTask ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(getApplicationContext(), errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("bcmPlanListingTask error ex", e.getMessage() + " ");
                        }
                        progressBar.setVisibility(View.GONE);

                    }
                }, "bcmPlanListingTask");
    }
}
