package com.emts.alacer.activity.superuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Status;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Srijana on 4/24/2017.
 */

public class StatusDetailActivity extends AppCompatActivity {
    Status status;
    TextView errorText;
    ProgressBar progressBar;
    ScrollView scrollView;
    SimpleDateFormat sdf1;
    SimpleDateFormat sdf2;
    SimpleDateFormat sdf;
    String totalResponded, totalNotResponded;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Status");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final Intent intent = getIntent();
        status = (Status) intent.getSerializableExtra("notification");

        scrollView = (ScrollView) findViewById(R.id.scrol_status);
        errorText = (TextView) findViewById(R.id.error_mesg);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf1 = new SimpleDateFormat("dd MMM yyyy");
        sdf2 = new SimpleDateFormat("HH:MM a");

        ImageView icon = (ImageView) findViewById(R.id.status_icon);
        Picasso.with(StatusDetailActivity.this).load(status.getTypeImage()).into(icon);

        TextView title = (TextView) findViewById(R.id.status_title);
        title.setText(status.getTypeName());

        try {
            Date date1 = sdf.parse(status.getCreatedDate());
            TextView date = (TextView) findViewById(R.id.status_date);
            date.setText(sdf2.format(date1) + " " + sdf1.format(date1));
        } catch (ParseException e) {
            Logger.e("status detail", e.getMessage());
        }

        TextView mesg = (TextView) findViewById(R.id.status_message);
        mesg.setText(status.getContent());

        if (NetworkUtils.isInNetwork(this)) {
            getStatusDetailTask();
            errorText.setVisibility(View.GONE);
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        }
        Button resend = (Button) findViewById(R.id.resend_to_nt_respond);
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendMassNotification();
            }
        });

        CardView cardResponded = (CardView) findViewById(R.id.card_responded);
        cardResponded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), RespondedListingActivity.class);
                intent1.putExtra("status", status);
                intent1.putExtra("isResponded", true);
                intent1.putExtra("total_responded", totalResponded);
                startActivity(intent1);
            }
        });
        CardView cardNotResponded = (CardView) findViewById(R.id.card_not_responded);
        cardNotResponded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), RespondedListingActivity.class);
                intent1.putExtra("status", status);
                intent1.putExtra("total_not_responded", totalNotResponded);
                startActivity(intent1);
            }
        });
    }

    private void resendMassNotification() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(StatusDetailActivity.this, "Resending the Mass notification");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("report_id", status.getStatusId());
        postParams.put("incident_id", status.getId());
        vHelper.addVolleyRequestListeners(Api.getInstance().resendMassNotification, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            AlertUtils.showSnack(StatusDetailActivity.this, errorText, res.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        pDialog.dismiss();

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                    }
                }, "resendMassNotification");
    }

    private void getStatusDetailTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("report_id", status.getStatusId());
        postParams.put("branch_id", status.getBranchId());
        postParams.put("department_id", status.getDepartId());

        vHelper.addVolleyRequestListeners(Api.getInstance().massNotificationStatusDetail, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject eachObj = new JSONObject(response);
                            if (eachObj.getBoolean("status")) {

                                TextView sent = (TextView) findViewById(R.id.total_sent);
                                sent.setText(eachObj.getString("total_sent"));

                                TextView respond = (TextView) findViewById(R.id.responded);
                                totalResponded = eachObj.getString("total_response");
                                respond.setText(totalResponded);

                                TextView ntRespond = (TextView) findViewById(R.id.not_resp);
                                totalNotResponded = String.valueOf(Integer.parseInt(eachObj.getString("total_sent")) - Integer.parseInt(eachObj.getString("total_response")));
                                ntRespond.setText(totalNotResponded);

                                float respondPercentage = ((float) Integer.parseInt(eachObj.getString("total_response")) / (Integer.parseInt(eachObj.getString("total_sent")))) * 100;
                                TextView respondedPer = (TextView) findViewById(R.id.tv_responded);
                                respondedPer.setText((int) respondPercentage + " %");

                                TextView safeAtWork = (TextView) findViewById(R.id.safe_at_work);
                                safeAtWork.setText(eachObj.getString("total_safe_at_work"));

                                TextView safeAwayWork = (TextView) findViewById(R.id.safe_away_work);
                                safeAwayWork.setText(eachObj.getString("total_safe_away_from_work"));

                                TextView needAssist = (TextView) findViewById(R.id.need_assist);
                                needAssist.setText(eachObj.getString("total_need_assistance"));
                                scrollView.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorText.setText(errorObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            Logger.e("inboxListingTask error ex", e.getMessage() + " ");
                        }
                        progressBar.setVisibility(View.GONE);

                    }
                }, "getStatusDetailTask");

    }
}
