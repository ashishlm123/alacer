package com.emts.alacer.activity.superuser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.NotificationStatusAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Notification;
import com.emts.alacer.model.Report;
import com.emts.alacer.model.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationStatusListing extends AppCompatActivity {
    RecyclerView notiList;
    ArrayList<Status> mList;
    ProgressBar progressBar;
    TextView errorText;
    PreferenceHelper pHelper;
    NotificationStatusAdapter adapter;
    Report report = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_status_listing);


        report = (Report) getIntent().getSerializableExtra("report");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(report.getTypeName() + " incident status");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mList = new ArrayList<>();
        pHelper = PreferenceHelper.getInstance(this);

        progressBar = (ProgressBar) findViewById(R.id.progress);
        errorText = (TextView) findViewById(R.id.error_mesg);

        notiList = (RecyclerView) findViewById(R.id.inbox_item_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        notiList.setLayoutManager(linearLayoutManager);

        adapter = new NotificationStatusAdapter(this, mList);
        notiList.setAdapter(adapter);

        if (NetworkUtils.isInNetwork(this) && report != null) {
            notiList.setVisibility(View.GONE);
            notificationStatusListingTask();
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            notiList.setVisibility(View.GONE);
        }
    }

    private void notificationStatusListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("type_id", report.getId());

        vHelper.addVolleyRequestListeners(Api.getInstance().notificationStatusListing, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray inboxArray = null;
                        try {
                           inboxArray = resObj.getJSONArray("incidents");
                        }catch (JSONException e){
                            progressBar.setVisibility(View.GONE);
                            notiList.setVisibility(View.GONE);
                            errorText.setText(resObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                            return;
                        }
                        for (int i = 0; i < inboxArray.length(); i++) {
                            JSONObject eachObj = inboxArray.getJSONObject(i);
                            Status status = new Status();
                            status.setStatusId(eachObj.getString("id"));
                            status.setTypeName(eachObj.getString("type_name"));
                            status.setContent(eachObj.getString("message"));
                            status.setId(eachObj.getString("incident_id"));
                            status.setDepartId(eachObj.getString("department_id"));
                            status.setBranchId(eachObj.getString("branch_id"));
                            status.setTotalSend(eachObj.getString("total_sent"));
                            status.setTotalResponse(eachObj.getString("total_response"));
                            status.setCreatedDate(eachObj.getString("created_date"));
                            if(!TextUtils.isEmpty(eachObj.getString("type_image"))){
                                status.setTypeImage(resObj.getString("icon_dir")+ eachObj.getString("type_image"));
                            }
                            mList.add(status);


                        }
                        adapter.notifyDataSetChanged();
                        notiList.setVisibility(View.VISIBLE);

                    } else {
                        AlertUtils.showSnack(NotificationStatusListing.this, notiList, resObj.getString("message"));
                        notiList.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("notificationStatusListingTask ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    AlertUtils.showSnack(NotificationStatusListing.this, notiList, errorObj.getString("message"));
                    notiList.setVisibility(View.GONE);
                    errorText.setText(errorObj.getString("message"));
                    errorText.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    Logger.e("notificationStatusListingTask error ex", e.getMessage() + " ");
                }
                progressBar.setVisibility(View.GONE);
            }
        }, "notificationStatusListingTask");
    }
}
