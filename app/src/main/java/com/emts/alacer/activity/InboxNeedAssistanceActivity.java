package com.emts.alacer.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.model.Notification;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Srijana on 4/21/2017.
 */

public class InboxNeedAssistanceActivity extends AppCompatActivity {
    String phoneNumber = "";
    private static final int REQUEST_CALL_PERMISSION = 1234;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inbx_need_assistance);

        Toolbar toolbar = (Toolbar) findViewById(R.id.need_assist_toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Inbox");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
        }
        assert intent != null;
        Notification notification = (Notification) intent.getSerializableExtra("notification");
        ImageView typeImage = (ImageView) findViewById(R.id.type_image);
        if (!TextUtils.isEmpty(notification.getImageUrl())) {
            Picasso.with(this).load(notification.getImageUrl()).into(typeImage);
        }

        String assistanceResponse = intent.getStringExtra("assistance_response");
        try {
            JSONObject assistanceObj = new JSONObject(assistanceResponse);
            JSONObject commanderObj = assistanceObj.getJSONObject("commander_data");
            TextView tvCommanderName = (TextView) findViewById(R.id.tv_commander_name);
            tvCommanderName.setText(commanderObj.getString("name"));
            TextView tvTitle = (TextView) findViewById(R.id.title);
            tvTitle.setText(notification.getTitle());
            phoneNumber = commanderObj.getString("contact_no");
            ImageView call = (ImageView) findViewById(R.id.iv_phone);
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(InboxNeedAssistanceActivity.this, Manifest.permission.CALL_PHONE)
                            == PackageManager.PERMISSION_GRANTED) {
                        phoneCall();
                    } else {
                        ActivityCompat.requestPermissions(InboxNeedAssistanceActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                REQUEST_CALL_PERMISSION);
                    }
                }
            });
            ImageView email = (ImageView) findViewById(R.id.iv_email);
            email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    emailApp();
                }
            });
            ImageView message = (ImageView) findViewById(R.id.iv_message);
            message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendMessage();
                }
            });
        } catch (JSONException e) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(InboxNeedAssistanceActivity.this);
            alertDialog.setMessage("No commander Assigned for your department and branch");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onBackPressed();
                    dialogInterface.dismiss();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
            Logger.e("assistanceRes ex", e.getMessage() + "");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    phoneCall();
                } else {
                    AlertUtils.showToast(InboxNeedAssistanceActivity.this, "Permission Denied !!!");
                }
            }
        }
    }

    private void sendMessage() {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + phoneNumber));
        sendIntent.putExtra("address", phoneNumber);
        try {
            startActivity(Intent.createChooser(sendIntent,
                    "Send sms using..."));
        } catch (android.content.ActivityNotFoundException ex) {
            AlertUtils.showToast(InboxNeedAssistanceActivity.this,
                    "No messaging app installed.");
        }
    }

    private void phoneCall() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    public void emailApp() {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{
                "support@commander.com"
        });
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Email Commander For Assistance");
        emailIntent.setType("message/rfc822");
        try {
            startActivity(Intent.createChooser(emailIntent,
                    "Send email using..."));
        } catch (android.content.ActivityNotFoundException ex) {
            AlertUtils.showToast(InboxNeedAssistanceActivity.this,
                    "No email clients installed.");
        }
    }
}
