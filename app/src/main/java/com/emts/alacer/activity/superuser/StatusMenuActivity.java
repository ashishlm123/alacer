package com.emts.alacer.activity.superuser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.emts.alacer.R;
import com.emts.alacer.activity.UserMainActivity;
import com.emts.alacer.fragment.Reporting;

public class StatusMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Status");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Reporting reportFrag = new Reporting();
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_superuser", true);
        bundle.putBoolean("is_status_check", true);
        reportFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_status_menu, reportFrag).commit();
    }
}
