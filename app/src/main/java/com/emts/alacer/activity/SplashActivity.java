package com.emts.alacer.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.activity.superuser.SuperUserDashboard;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Srijana on 4/19/2017.
 */

public class SplashActivity extends AppCompatActivity {
    PreferenceHelper prefHelper;
    Handler handler;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (prefHelper.isLogin()) {
                if (prefHelper.getUserType().equals(PreferenceHelper.APP_USER_USER)) {
                    startActivity(new Intent(getApplicationContext(), UserMainActivity.class));
                } else if (prefHelper.getUserType().equals(PreferenceHelper.APP_USER_SUPER_USER)) {
                    startActivity(new Intent(getApplicationContext(), SuperUserDashboard.class));
                } else {
                    prefHelper.edit().clear().commit();
                    startActivity(new Intent(getApplicationContext(), UserSelectActivity.class));
                }
            } else {
                startActivity(new Intent(getApplicationContext(), UserSelectActivity.class));
            }
            SplashActivity.this.finish();

//            doFileUpload();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefHelper = PreferenceHelper.getInstance(this);

        if (prefHelper.isLogin()) {
            if (prefHelper.getBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, false)) {

                updateDeviceTokenTask(SplashActivity.this);
            }
        }
        handler = new Handler();
        handler.postDelayed(runnable, 2400);

        Logger.e("device id", FirebaseInstanceId.getInstance().getToken() + "   ");
    }

    public void updateDeviceTokenTask(Context context) {
        prefHelper = PreferenceHelper.getInstance(SplashActivity.this);
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("token", prefHelper.getToken());
        postParams.put("user_id", String.valueOf(prefHelper.getUserId()));
        if (!TextUtils.isEmpty(FirebaseInstanceId.getInstance().getToken()) || !FirebaseInstanceId.getInstance().getToken().equals(null)) {
            postParams.put("device_id", FirebaseInstanceId.getInstance().getToken());
            Logger.e("devic_update", postParams.get("device_id"));
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().upgradeDevicetoken, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                prefHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, false).commit();
                            } else {
                                prefHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();

                            }
                        } catch (JSONException e) {

                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        prefHelper.edit().putBoolean(PreferenceHelper.GCM_TOKEN_UPDATE, true).commit();
                    }
                }, " updateDeviceTokenTask");


    }


//    private void doFileUpload() {
//        HttpURLConnection conn = null;
//        DataOutputStream dos = null;
//        DataInputStream inStream = null;
//        String lineEnd = "rn";
//        String twoHyphens = "--";
//        String boundary = "*****";
//        int bytesRead, bytesAvailable, bufferSize;
//        byte[] buffer;
//        int maxBufferSize = 1 * 1024 * 1024;
//        String responseFromServer = "";
//        String urlString = "http://10.0.2.2/check.php";
//        try {
//            //------------------ CLIENT REQUEST
//            File file = new File("/storage/emulated/Music/Alacer/VID_20170529_105306.mp4");
//            FileInputStream fileInputStream=null;
//            if (file.exists()) {
//                fileInputStream = new FileInputStream(file);
//                Logger.e("exit","yes");
//            }
//            Logger.e("no","file is not present");
//            // open a URL connection to the Servlet
//            URL url = new URL(urlString);
//            // Open a HTTP connection to the URL
//            conn = (HttpURLConnection) url.openConnection();
//            // Allow Inputs
//            conn.setDoInput(true);
//            // Allow Outputs
//            conn.setDoOutput(true);
//            // Don't use a cached copy.
//            conn.setUseCaches(false);
//            // Use a post method.
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Connection", "Keep-Alive");
//            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
//            dos = new DataOutputStream(conn.getOutputStream());
//            dos.writeBytes(twoHyphens + boundary + lineEnd);
////            dos.writeBytes("Content-Disposition: form-data; name="uploadedfile"filename="" + selectedPath + """ + lineEnd);
//            dos.writeBytes(lineEnd);
//            // create a buffer of maximum size
//            bytesAvailable = fileInputStream.available();
//            bufferSize = Math.min(bytesAvailable, maxBufferSize);
//            buffer = new byte[bufferSize];
//            // read file and write it into form...
//            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//            while (bytesRead > 0) {
//                dos.write(buffer, 0, bufferSize);
//                bytesAvailable = fileInputStream.available();
//                bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//            }
//            // send multipart form data necesssary after file data...
//            dos.writeBytes(lineEnd);
//            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
//            // close streams
//            Log.e("Debug", "File is written");
//            fileInputStream.close();
//            dos.flush();
//            dos.close();
//        } catch (MalformedURLException ex) {
//            Log.e("Debug", "error: " + ex.getMessage(), ex);
//        } catch (IOException ioe) {
//            Log.e("Debug", "error: " + ioe.getMessage(), ioe);
//        }
//        //------------------ read the SERVER RESPONSE
//        try {
//            inStream = new DataInputStream(conn.getInputStream());
//            String str;
//
//            while ((str = inStream.readLine()) != null) {
//                Log.e("Debug", "Server Response " + str);
//            }
//            inStream.close();
//
//        } catch (IOException ioex) {
//            Log.e("Debug", "error: " + ioex.getMessage(), ioex);
//        }
//    }

}

