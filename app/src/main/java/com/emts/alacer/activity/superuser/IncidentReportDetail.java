package com.emts.alacer.activity.superuser;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.AudioService;
import com.emts.alacer.R;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Branch;
import com.emts.alacer.model.Department;
import com.emts.alacer.model.Incident;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Srijana on 4/24/2017.
 */

public class IncidentReportDetail extends AppCompatActivity {
    Incident incident;
    Spinner spinRecp;

    String[] recipient = new String[]{"RECIPIENT", "EMPLOYEES", "BRANCH", "DEPARTMENT"};
    ArrayList<Department> departs;
    ArrayList<Branch> branches;
    ImageView inIcon, media, audio;
    TextView inTitle, inDate, inScale, message, location, errorRecp, errorNot, spinBranch, spinDept,
            depList, branchList;
    CardView sms, email, mobile;
    CheckBox rSms, rEmail, rMobile;
    Button launch;
    AudioService mService;
    Handler mHandler;
    boolean playing = false;
    CircularProgressBar circularProgressBar;
    SimpleDateFormat sdf, sdf1, sdf2;
    Typeface typeface;
    boolean fromPush;
    String[] department, branch;
    boolean[] checkedDepart, checkedBranch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_incident_report_detail);

        departs = new ArrayList<>();
        branches = new ArrayList<>();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Incident Report");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final Intent intent = getIntent();
        if (intent != null) {
            incident = (Incident) intent.getSerializableExtra("incident");
            fromPush = intent.getBooleanExtra("from_push", false);
        }

        mHandler = new Handler();

        inIcon = (ImageView) findViewById(R.id.incident_icon);
        media = (ImageView) findViewById(R.id.img_media);
        inTitle = (TextView) findViewById(R.id.incident_title);
        inDate = (TextView) findViewById(R.id.incident_date);
        inScale = (TextView) findViewById(R.id.scale_value);
        message = (TextView) findViewById(R.id.incident_message);
        location = (TextView) findViewById(R.id.incident_location);
        sms = (CardView) findViewById(R.id.lay_sms);
        email = (CardView) findViewById(R.id.lay_email);
        mobile = (CardView) findViewById(R.id.lay_mob);
        rSms = (CheckBox) findViewById(R.id.rad_sms);
        rEmail = (CheckBox) findViewById(R.id.rad_email);
        rMobile = (CheckBox) findViewById(R.id.rad_mob);
        launch = (Button) findViewById(R.id.launch);
        errorNot = (TextView) findViewById(R.id.validate_not);
        errorRecp = (TextView) findViewById(R.id.validate_recp);
        audio = (ImageView) findViewById(R.id.img_voice);
        circularProgressBar = (CircularProgressBar) findViewById(R.id.cbProgress);

        sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf1 = new SimpleDateFormat("dd MMM yyyy");
        sdf2 = new SimpleDateFormat("HH:MM a");
        audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mService != null && !TextUtils.isEmpty(incident.getAudioUrl())) {
                    if (playing) {
                        audio.setImageResource(R.drawable.btn_play);
                        mService.pausePlayer();
                        playing = false;
                    } else {
                        audio.setImageResource(R.drawable.btn_hold);
                        mService.setAudioUrl(incident.getAudioUrl());
                        mService.playSong();
                        playing = true;
                    }
                } else {
                    AlertUtils.showSnack(IncidentReportDetail.this, view, "No Audio Available");
                }

            }
        });


        spinRecp = (Spinner) findViewById(R.id.recipient);
        spinBranch = (TextView) findViewById(R.id.spin_branch);
        spinDept = (TextView) findViewById(R.id.spin_department);
        final LinearLayout branch = (LinearLayout) findViewById(R.id.lay_spin_branch);
        final LinearLayout dept = (LinearLayout) findViewById(R.id.lay_spin_dept);
        depList = (TextView) findViewById(R.id.tv_dept_list);
        branchList = (TextView) findViewById(R.id.tv_branch_list);

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rSms.isChecked()) {
                    rSms.setChecked(false);
                } else {
                    rSms.setChecked(true);
                }
            }
        });


        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rEmail.isChecked()) {
                    rEmail.setChecked(false);
                } else {
                    rEmail.setChecked(true);
                }
            }
        });
        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rMobile.isChecked()) {
                    rMobile.setChecked(false);
                } else {
                    rMobile.setChecked(true);
                }
            }
        });


        inTitle.setText(incident.getTypeName());

        try {
            Date date = sdf.parse(incident.getCreatedDate());
            inDate.setText(sdf2.format(date) + "  " + sdf1.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        inScale.setText(incident.getScale());
        message.setText(incident.getMesg());
        location.setText(incident.getLocation());
        Picasso.with(getApplicationContext()).load(incident.getImageUrl()).into(inIcon);
        Picasso.with(getApplicationContext()).load(incident.getMedia()).into(media);


        launch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isvalid = true;
                if (rSms.isChecked() || rEmail.isChecked() || rMobile.isChecked()) {
                    errorNot.setVisibility(View.GONE);
                } else {
                    isvalid = false;
                    errorNot.setText("Please choose one notification method");
                    errorNot.setVisibility(View.VISIBLE);
                }
                if (spinRecp.getSelectedItemPosition() == 0) {
                    isvalid = false;
                    errorRecp.setText("Please Choose valid recipient");
                    errorRecp.setVisibility(View.VISIBLE);
                } else {
                    if (spinRecp.getSelectedItemPosition() == 2) {
                        boolean isAnyBranchChecked = false;
                        //branch
                        for (int i = 0; i < checkedBranch.length; i++) {
                            if (checkedBranch[i]) {
                                isAnyBranchChecked = true;
                            }
                        }
                        if (!isAnyBranchChecked) {
                            isvalid = false;
                            errorRecp.setText("Please select atleast one branch");
                            errorRecp.setVisibility(View.VISIBLE);
                        } else {
                            errorRecp.setVisibility(View.GONE);
                        }
                    } else if (spinRecp.getSelectedItemPosition() == 3) {
                        //department
                        boolean isAnyDepartChecked = false;
                        //branch
                        for (int i = 0; i < checkedDepart.length; i++) {
                            if (checkedDepart[i]) {
                                isAnyDepartChecked = true;
                            }
                        }
                        if (!isAnyDepartChecked) {
                            isvalid = false;
                            errorRecp.setText("Please select atleast one department");
                            errorRecp.setVisibility(View.VISIBLE);
                        } else {
                            errorRecp.setVisibility(View.GONE);
                        }
                    } else {
                        errorRecp.setVisibility(View.GONE);
                    }
                }
                if (isvalid) {
                    if (NetworkUtils.isInNetwork(IncidentReportDetail.this)) {
                        sendMassNotificationTask();
                    } else {
                        AlertUtils.showSnack(getApplicationContext(), toolbar, "No Internet Connected");
                    }
                }

            }
        });

        if (NetworkUtils.isInNetwork(this)) {
            getDepartmentTask();
            getBranchTask();
        } else {
            AlertUtils.showSnack(this, toolbar, "No Internet Connected !!!");
        }

        typeface = Typeface.createFromAsset(getAssets(), "fonts/opensans_regular.ttf");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(IncidentReportDetail.this,
                R.layout.item_spinner_signup, recipient) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = LayoutInflater.from(IncidentReportDetail.this).inflate(R.layout.item_spinner_signup, parent, false);
                ((TextView) view).setTypeface(typeface);
                ((TextView) view).setText(recipient[position]);
                return view;
            }
        };
        spinRecp.setAdapter(adapter);

        spinRecp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (spinRecp.getSelectedItemPosition() == 2) {
                    Arrays.fill(checkedDepart, false); //set all depart unchecked
                    branch.setVisibility(View.VISIBLE);
                    dept.setVisibility(View.GONE);
                    branchList.setVisibility(View.GONE);
                    depList.setVisibility(View.GONE);
                } else if (spinRecp.getSelectedItemPosition() == 3) {
                    Arrays.fill(checkedBranch, false);//set all branch unchecked
                    dept.setVisibility(View.VISIBLE);
                    branch.setVisibility(View.GONE);
                    branchList.setVisibility(View.GONE);
                    depList.setVisibility(View.GONE);
                } else {
                    dept.setVisibility(View.GONE);
                    branch.setVisibility(View.GONE);
                    branchList.setVisibility(View.GONE);
                    depList.setVisibility(View.GONE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void sendMassNotificationTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(IncidentReportDetail.this, "Launching...");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postparam = vHelper.getPostParams();
        postparam.put("incident_id", incident.getId());
        postparam.put("type_id", incident.getTypeId());
        postparam.put("location", incident.getLocation());
        postparam.put("message", incident.getMesg());
        postparam.put("longitude", incident.getLongitude());
        postparam.put("latitude", incident.getLatitude());
        postparam.put("recipient", spinRecp.getSelectedItem().toString().toLowerCase());
        if (spinRecp.getSelectedItemPosition() == 3) {
            int index = 0;
            for (int i = 0; i < checkedDepart.length; i++) {
                if (checkedDepart[i]) {
                    postparam.put("department_ids[" + index + "]", departs.get(i).getDepartId());
                    index++;
                }
            }
        }
//        else {
//            postparam.put("department_ids[0]", "0");
//        }
        if (spinRecp.getSelectedItemPosition() == 2) {
            int index = 0;
            for (int i = 0; i < checkedBranch.length; i++) {
                if (checkedBranch[i]) {
                    postparam.put("branch_ids[" + index + "]", branches.get(i).getBranchId());
                    index++;
                }
            }
        }
//        else {
//            postparam.put("branch_ids[0]", "0");
//        }
        if (rSms.isChecked()) {
            postparam.put("sms", "sms");
        } else {
            postparam.put("sms", "");
        }
        if (rEmail.isChecked()) {
            postparam.put("email", "email");
        } else {
            postparam.put("email", "");
        }
        if (rMobile.isChecked()) {
            postparam.put("mobile_notification", "mobile");
        } else {
            postparam.put("mobile_notification", "");
        }
        HashMap<String, String> dataPost = new HashMap<>();
//        if (!TextUtils.isEmpty(AudioSavePathInDevice)) {
//            dataPost.put("audio", AudioSavePathInDevice);
//            Logger.e("audiopath",AudioSavePathInDevice);
//        }
        vHelper.addVolleyRequestListeners(Api.getInstance().massNotificationUrl, Request.Method.POST, postparam,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {

                        try {
                            JSONObject reObj = new JSONObject(response);
                            if (reObj.getBoolean("status")) {
                                AlertUtils.showAlertMessage(IncidentReportDetail.this, "Message", reObj.getString("message"));
                            } else {
                                AlertUtils.showAlertMessage(IncidentReportDetail.this, "Message", reObj.getString("message"));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(IncidentReportDetail.this, errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("sendMassNotificationTask error ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }
                }, "sendMassNotificationTask");
    }

    private void getDepartmentTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getDepartmentUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray departArray = resObj.getJSONArray("department_list");
                                for (int i = 0; i < departArray.length(); i++) {
                                    JSONObject eachObj = departArray.getJSONObject(i);
                                    Department depart = new Department();
                                    depart.setDepartName(eachObj.getString("name"));
                                    depart.setDepartId(eachObj.getString("id"));

                                    departs.add(depart);
                                }
                                department = new String[departs.size()];
                                checkedDepart = new boolean[departs.size()];
                                spinDept.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showDepartmentListAlert(departs, department, checkedDepart);
                                    }
                                });
                            }
//                            departLoad = true;
//                            checkProgress();
                        } catch (JSONException e) {
                            Logger.e("getDepart res ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
//                        departLoad = true;
//                        checkProgress();
                    }
                }, "getDepartmentTask");
    }

    private void showDepartmentListAlert(final ArrayList<Department> departs, final String[] department, final boolean[] checkedDepart) {
        AlertDialog.Builder builder = new AlertDialog.Builder(IncidentReportDetail.this);

        // String array for alert dialog multi choice items
        for (int i = 0; i < departs.size(); i++) {
            department[i] = departs.get(i).getDepartName();

            // Boolean array for initial selected items
            if (checkedDepart[i]) {
                checkedDepart[i] = true;
            } else {
                checkedDepart[i] = false;
            }
        }

        // Convert the color array to list
        final List<String> colorsList = Arrays.asList(department);

        // Set multiple choice items for alert dialog
                /*
                    AlertDialog.Builder setMultiChoiceItems(CharSequence[] items, boolean[]
                    checkedItems, DialogInterface.OnMultiChoiceClickListener listener)
                        Set a list of items to be displayed in the dialog as the content,
                        you will be notified of the selected item via the supplied listener.
                 */
                /*
                    DialogInterface.OnMultiChoiceClickListener
                    public abstract void onClick (DialogInterface dialog, int which, boolean isChecked)

                        This method will be invoked when an item in the dialog is clicked.

                        Parameters
                        dialog The dialog where the selection was made.
                        which The position of the item in the list that was clicked.
                        isChecked True if the click checked the item, else false.
                 */
        builder.setMultiChoiceItems(department, checkedDepart, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedDepart[which] = isChecked;

                // Get the current focused item
                String currentItem = colorsList.get(which);

                // Notify the current action
//                Toast.makeText(getApplicationContext(),
//                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Select Department");

        // Set the positive/yes button click listener
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                depList.setText(" ");
                String departName = "";
                for (int i = 0; i < checkedDepart.length; i++) {
                    boolean checked = checkedDepart[i];
                    if (checked) {
                        checkedDepart[i] = true;
                        if (departName.equals("")) {
                            departName = departs.get(i).getDepartName();
                        } else {
                            departName = departName + "\n" + departs.get(i).getDepartName();
                        }
                    }
                }
                if (!TextUtils.isEmpty(departName.trim())) {
                    depList.setText(departName.trim());
                    depList.setVisibility(View.VISIBLE);
                } else {
                    depList.setVisibility(View.GONE);
                }
                Logger.e("depart_nme", departName + " ");

            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    private void getBranchTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getBranchUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray departArray = resObj.getJSONArray("branch_list");
                                for (int i = 0; i < departArray.length(); i++) {
                                    JSONObject eachObj = departArray.getJSONObject(i);
                                    Branch branch = new Branch();
                                    branch.setBranchName(eachObj.getString("name"));
                                    branch.setBranchId(eachObj.getString("id"));

                                    branches.add(branch);
                                }
                                branch = new String[branches.size()];
                                checkedBranch = new boolean[branches.size()];
                                spinBranch.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showBranchListAlert(branches, branch, checkedBranch);
                                    }
                                });
                            }
//                            branchLoad = true;
//                            checkProgress();
                        } catch (JSONException e) {
                            Logger.e("getBranch res ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
//                        branchLoad = true;
//                        checkProgress();
                    }
                }, "getBranchTask");
    }

    private void showBranchListAlert(final ArrayList<Branch> branches, String[] branch, final boolean[] checkedBranch) {
        AlertDialog.Builder builder = new AlertDialog.Builder(IncidentReportDetail.this);

        // String array for alert dialog multi choice items
        for (int i = 0; i < branches.size(); i++) {
            branch[i] = branches.get(i).getBranchName();

            // Boolean array for initial selected items
            if (checkedBranch[i]) {
                checkedBranch[i] = true;
            } else {
                checkedBranch[i] = false;
            }
        }

        // Convert the color array to list
        final List<String> colorsList = Arrays.asList(branch);

        // Set multiple choice items for alert dialog
                /*
                    AlertDialog.Builder setMultiChoiceItems(CharSequence[] items, boolean[]
                    checkedItems, DialogInterface.OnMultiChoiceClickListener listener)
                        Set a list of items to be displayed in the dialog as the content,
                        you will be notified of the selected item via the supplied listener.
                 */
                /*
                    DialogInterface.OnMultiChoiceClickListener
                    public abstract void onClick (DialogInterface dialog, int which, boolean isChecked)

                        This method will be invoked when an item in the dialog is clicked.

                        Parameters
                        dialog The dialog where the selection was made.
                        which The position of the item in the list that was clicked.
                        isChecked True if the click checked the item, else false.
                 */
        builder.setMultiChoiceItems(branch, checkedBranch, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedBranch[which] = isChecked;

                // Get the current focused item
                String currentItem = colorsList.get(which);

                // Notify the current action
//                Toast.makeText(getApplicationContext(),
//                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Select Branch");

        // Set the positive/yes button click listener
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click positive button
                branchList.setText(" ");
                String departName = "";
                for (int i = 0; i < checkedBranch.length; i++) {
                    boolean checked = checkedBranch[i];
                    if (checked) {
                        checkedBranch[i] = true;
                        if (departName.equals("")) {
                            departName = branches.get(i).getBranchName();
                        } else {
                            departName = departName + "\n" + branches.get(i).getBranchName();
                        }
                    }
                }
                if (!TextUtils.isEmpty(departName.trim())) {
                    branchList.setText(departName);
                    branchList.setVisibility(View.VISIBLE);
                } else {
                    branchList.setVisibility(View.GONE);
                }
                Logger.e("branch list _nme", departName + " ");

            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }

    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) service;
            //get service
            mService = binder.getService();
            mService.setAudioUrl(incident.getAudioUrl());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        Intent playIntent = new Intent(this, AudioService.class);
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
//        startService(playIntent);
        mHandler.postDelayed(runnable, 1000);
    }

    @Override
    protected void onPause() {
        try {
            unbindService(musicConnection);
            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        try {
            unbindService(musicConnection);
            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
        }
        if (fromPush) {
            Intent intent = new Intent(getApplicationContext(), SuperUserDashboard.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return;
        }
        super.onBackPressed();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mService != null) {
                int progress = mService.mProgress;
                if (progress > 100) {
                    circularProgressBar.setProgress(100);

                } else if (progress < 0) {
                    circularProgressBar.setProgress(0);
                } else {
                    circularProgressBar.setProgress(progress);
                }
                Logger.e("progress update", mService.mProgress + " ");
                if (circularProgressBar.getProgress() == 100) {
                    audio.setImageResource(R.drawable.btn_play);
                    circularProgressBar.setProgress(0);
                    mService.mProgress = 0;
                    playing = false;
                }

//                if (circularProgressBar.getProgress() == 100 || circularProgressBar.getProgress() == 0) {
//                    audio.setImageResource(R.drawable.btn_play);
//                    circularProgressBar.setProgress(0);
//                    mService.mProgress = 0;
//                    playing = false;
//                } else {
//                    audio.setImageResource(R.drawable.btn_hold);
//                }
                mHandler.postDelayed(runnable, 1000);
            }
        }
    };
}
