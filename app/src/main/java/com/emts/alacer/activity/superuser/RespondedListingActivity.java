package com.emts.alacer.activity.superuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.RespondedAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Status;
import com.emts.alacer.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RespondedListingActivity extends AppCompatActivity {
    public boolean isResponded;
    ProgressBar progressBar;
    TextView errorView;
    RecyclerView respondListView;
    RespondedAdapter adapter;
    Status status;
    ArrayList<User> respondArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responded_detail);

        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
            return;
        }
        isResponded = intent.getBooleanExtra("isResponded", false);
        status = (Status) intent.getSerializableExtra("status");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView tvTotalResponded = (TextView) findViewById(R.id.status_no_lvl);
        if (isResponded) {
            ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Responded");
            tvTotalResponded.setText("No. of Responded: " + intent.getStringExtra("total_responded"));
        } else {
            ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText("Not Responded");
            tvTotalResponded.setText("No. of Not Responded: " + intent.getStringExtra("total_not_responded"));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progress);
        errorView = (TextView) findViewById(R.id.error_view);
        respondListView = (RecyclerView) findViewById(R.id.list_responded);
        respondListView.setLayoutManager(new LinearLayoutManager(this));


        adapter = new RespondedAdapter(this, respondArray);
        respondListView.setAdapter(adapter);
        if (NetworkUtils.isInNetwork(this)) {
            getRespondedListTask();
            errorView.setVisibility(View.GONE);
        } else {
            errorView.setText("No Internet Connected !!");
            errorView.setVisibility(View.VISIBLE);
            respondListView.setVisibility(View.GONE);
        }

    }

    private void getRespondedListTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("report_id", status.getStatusId());

        if (isResponded) {
            postParam.put("responded_type", "responded");
        } else {
            postParam.put("responded_type", "not_responded");
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().getRespondedUser, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray inboxArray = null;
                        try {
                            inboxArray = resObj.getJSONArray("users_list");
                            errorView.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                            respondListView.setVisibility(View.GONE);
                            errorView.setText(resObj.getString("message"));
                            errorView.setVisibility(View.VISIBLE);
                            return;
                        }
                        for (int i = 0; i < inboxArray.length(); i++) {
                            JSONObject eachObj = inboxArray.getJSONObject(i);
                            User user = new User();
                            user.setName(eachObj.getString("name"));
                            user.setMobile(eachObj.getString("mobile_no"));
                            user.setEmail(eachObj.getString("email"));
                            user.setDepartment(eachObj.getString("department"));
                            user.setBranch(eachObj.getString("branch"));
                            respondArray.add(user);


                        }
                        adapter.notifyDataSetChanged();
                        respondListView.setVisibility(View.VISIBLE);

                    } else {
                        AlertUtils.showSnack(RespondedListingActivity.this, respondListView, resObj.getString("message"));
                        respondListView.setVisibility(View.GONE);
                        errorView.setText(resObj.getString("message"));
                        errorView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("getRespondedListTask ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    AlertUtils.showSnack(RespondedListingActivity.this, respondListView, errorObj.getString("message"));
                    respondListView.setVisibility(View.GONE);
                    errorView.setText(errorObj.getString("message"));
                    errorView.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    Logger.e("getRespondedListTask error ex", e.getMessage() + " ");
                }
                progressBar.setVisibility(View.GONE);
            }
        }, "getRespondedListTask");
    }
}
