package com.emts.alacer.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.activity.BCMSearchActivity;
import com.emts.alacer.adapter.BCMMenuAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Report;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 4/21/2017.
 */

public class BCMPlan extends Fragment {
    Context context;
    //    String[] menuTitle;
    int[] menuColor;
    ArrayList<Report> bcList;
    RecyclerView bcmPlanList;
    BCMMenuAdapter adapter;
    ProgressBar progressBar;
    TextView errorText;

    public BCMPlan() {
        //empty constructor
    }

    public BCMPlan(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bcm_plan, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        menuTitle = new String[]{"EMERGENCY RESPONSE PLAN", "IT DISASTER RECOVERY PLAN", "BUSINESS CONTINUITY PLAN",
//                "CRISIS MANAGEMENT PLAN", "CRISIS COMMUNICATION PLAN", "SUPPLY CHAIN MANAGEMENT PLAN",
//                "COUNTRY EVACUATION PLAN", "POWER OUTAGES"};
        menuColor = new int[]{R.color.bg_emergency, R.color.bg_disaster,
                R.color.bg_business, R.color.bg_crisis_mgnt, R.color.bg_crisis_com, R.color.bg_supply_chain,
                R.color.bg_country, R.color.bg_power};

        bcList = new ArrayList<>();
        bcmPlanList = (RecyclerView) view.findViewById(R.id.bcmplan_menu);
        bcmPlanList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BCMMenuAdapter(bcList, context);
        bcmPlanList.setAdapter(adapter);

        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.error_mesg);

        if (NetworkUtils.isInNetwork(context)) {
            bcmPlanListingTask();
            bcmPlanList.setVisibility(View.GONE);
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            bcmPlanList.setVisibility(View.GONE);
        }
    }

    private void bcmPlanListingTask() {

        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().bcmPlan, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);

                            if (resObj.getBoolean("status")) {
                                JSONArray array = resObj.getJSONArray("bcm_plans");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject eachObj = array.getJSONObject(i);
                                    Report report = new Report();
                                    report.setTypeName(eachObj.getString("name"));
                                    if (!TextUtils.isEmpty(eachObj.getString("file_saved")))
                                        report.setTypeImage(resObj.getString("file_path") + eachObj.getString("file_saved"));
                                    bcList.add(report);
                                }
                                adapter.notifyDataSetChanged();
                                bcmPlanList.setVisibility(View.VISIBLE);
                            } else {
                                errorText.setText(resObj.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                bcmPlanList.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("bcmPlanListingTask ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(getActivity(), errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("bcmPlanListingTask error ex", e.getMessage() + " ");
                        }
                        progressBar.setVisibility(View.GONE);

                    }
                }, "bcmPlanListingTask");
    }

    public void dataTansfer() {
        Intent intent = new Intent(getActivity(), BCMSearchActivity.class);
        intent.putExtra("bcm", bcList);
        startActivity(intent);

    }

}
