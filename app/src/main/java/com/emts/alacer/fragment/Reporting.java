package com.emts.alacer.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.ReportMenuAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Report;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 4/21/2017.
 */

public class Reporting extends Fragment {
    Context context;
    //    String[] menuTitle;
//    int[] menuIcon;
    ArrayList<Report> rList;
    ReportMenuAdapter adapter;
    ProgressBar progressBar;
    TextView errorText;
    RecyclerView reportingList;
    boolean isSuperUser;
    boolean isStatusCheck;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context= getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            isSuperUser = bundle.getBoolean("is_superuser", false);
            isStatusCheck = bundle.getBoolean("is_status_check", false);
        } else {
            isSuperUser = false;
        }

        return inflater.inflate(R.layout.fragment_reporting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        menuTitle = new String[]{"POWER OUTAGES", "CYBER ATTACK", "FIRE",
//                "WEATHER", "SECURITY", "ACCIDENT/INJURY", "OTHER INCIDENT"};
//        menuIcon = new int[]{R.drawable.icon_poweroutages, R.drawable.icon_cyberattack,
//                R.drawable.icon_fire, R.drawable.icon_weather, R.drawable.icon_security, R.drawable.icon_accident,
//                R.drawable.icon_otherincident};

        rList = new ArrayList<>();

        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.error_mesg);
        reportingList = (RecyclerView) view.findViewById(R.id.reporting_menu);
        reportingList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ReportMenuAdapter(getActivity(), rList);
        if (isSuperUser) {
            adapter.setSuperUser(true);
            if (isStatusCheck){
                adapter.setStatusCheck(true);
            }
        } else {
            adapter.setSuperUser(false);
        }
        reportingList.setAdapter(adapter);

        if (NetworkUtils.isInNetwork(context)) {
            reportListingTask();
            errorText.setVisibility(View.GONE);
            reportingList.setVisibility(View.GONE);
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            reportingList.setVisibility(View.GONE);
        }

    }

    private void reportListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        final HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().reportingTypeUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);

                            if (resObj.getBoolean("status")) {
                                JSONArray reportArray = resObj.getJSONArray("reporting_type");

                                for (int i = 0; i < reportArray.length(); i++) {
                                    JSONObject eachObj = reportArray.getJSONObject(i);
                                    Report report = new Report();
                                    report.setId(eachObj.getString("id"));
                                    report.setTypeName(eachObj.getString("type_name"));
                                    if (!TextUtils.isEmpty(eachObj.getString("type_image"))) {
                                        report.setTypeImage(resObj.getString("icon_dir") + eachObj.getString("type_image"));
                                    }

                                    rList.add(report);
                                }
                                if (isSuperUser && !isStatusCheck) {
                                    Report report = new Report();
                                    report.setId("-1");
                                    report.setTypeName("STATUS");
                                    rList.add(report);
                                }
                                adapter.notifyDataSetChanged();
                                reportingList.setVisibility(View.VISIBLE);

                            } else {

                                errorText.setText(resObj.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                reportingList.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("reportListingTask ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.showToast(getActivity(), errorObj.getString("message"));
                        } catch (JSONException e) {
                            Logger.e("reportListingTask error ex", e.getMessage() + " ");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                }, "reportListingTask");
    }

}
