package com.emts.alacer.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.activity.CmsDetailActivity;
import com.emts.alacer.activity.UserSelectActivity;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Report;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 4/21/2017.
 */

public class More extends Fragment {

    Context context;
    String[] menuTitle;
    ProgressBar progressBar;
    TextView errorText;
    RecyclerView moreList;
    MoreMenuAdapter adapter;
    ArrayList<Report> mList;
    PreferenceHelper prefHelper;

    public More(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_more, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        menuTitle = new String[]{"ABOUT US", "TERM & CONDITION", "CONTACT US",
                "LOG OUT"};

        prefHelper = PreferenceHelper.getInstance(context);
        mList = new ArrayList<>();
        moreList = (RecyclerView) view.findViewById(R.id.more_menu);
        moreList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MoreMenuAdapter(mList);
        moreList.setAdapter(adapter);

        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.error_mesg);
        if (NetworkUtils.isInNetwork(context)) {
            getMoreListTask();
            moreList.setVisibility(View.GONE);
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            moreList.setVisibility(View.GONE);
        }
    }

    private void getMoreListTask() {

        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        final HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().cmsUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);

                            if (resObj.getBoolean("status")) {
                                JSONArray reportArray = resObj.getJSONArray("cms");

                                for (int i = 0; i < reportArray.length(); i++) {
                                    JSONObject eachObj = reportArray.getJSONObject(i);
                                    Report report = new Report();
                                    report.setId(eachObj.getString("id"));
                                    report.setTypeName(eachObj.getString("heading"));
                                    report.setContent(eachObj.getString("content"));
                                    mList.add(report);
                                }
                                Report report = new Report();
                                report.setTypeName("LOG OUT");
                                mList.add(report);

                                adapter.notifyDataSetChanged();
                                moreList.setVisibility(View.VISIBLE);

                            } else {
                                Report report = new Report();
                                report.setTypeName("LOG OUT");
                                mList.add(report);
                                adapter.notifyDataSetChanged();
                                moreList.setVisibility(View.VISIBLE);
                                AlertUtils.showToast(getActivity(), resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("getMoreListTask ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
//                            AlertUtils.showToast(getActivity(), errorObj.getString("message"));
                            Report report = new Report();
                            report.setTypeName("LOG OUT");
                            mList.add(report);

                            adapter.notifyDataSetChanged();
                            moreList.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            Logger.e("getMoreListTask error ex", e.getMessage() + " ");


                        }
                        progressBar.setVisibility(View.GONE);
                    }

                }, "getMoreListTask");
    }


    private class MoreMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ArrayList<Report> moreList;
        Typeface sansRegular;

        public MoreMenuAdapter(ArrayList<Report> moreList) {
            this.moreList = moreList;
            sansRegular = Typeface.createFromAsset(context.getAssets(), "fonts/opensans_regular.ttf");
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new More.MoreMenuAdapter.ViewHolder(LayoutInflater.from(context).inflate(
                    R.layout.item_super_user_menu, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MoreMenuAdapter.ViewHolder viewHolder = (MoreMenuAdapter.ViewHolder) holder;
            Report more = moreList.get(position);
            viewHolder.mTitle.setText(more.getTypeName().toUpperCase());
        }

        @Override
        public int getItemCount() {
            return moreList.size();
        }

        private class ViewHolder extends RecyclerView.ViewHolder {
            TextView mTitle;
            ImageView mImg;

            ViewHolder(View itemView) {
                super(itemView);

                mTitle = (TextView) itemView.findViewById(R.id.menu_title);
                mTitle.setTextColor(getResources().getColor(R.color.more_list_text_color));
                mTitle.setTypeface(sansRegular);
                mImg = (ImageView) itemView.findViewById(R.id.menu_img);
                mImg.setVisibility(View.GONE);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (getLayoutPosition() == moreList.size() - 1) {
                            prefHelper.edit().clear().commit();
                            Intent intent1 = new Intent(context, UserSelectActivity.class);
                            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent1);

                        } else {
                            Intent intent = new Intent(context, CmsDetailActivity.class);
                            intent.putExtra("more", moreList.get(getLayoutPosition()));
                            context.startActivity(intent);
                        }
                    }
                });


            }
        }
    }
}
