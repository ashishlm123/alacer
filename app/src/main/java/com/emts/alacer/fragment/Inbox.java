package com.emts.alacer.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.alacer.R;
import com.emts.alacer.adapter.NotificationAdapter;
import com.emts.alacer.helper.AlertUtils;
import com.emts.alacer.helper.Api;
import com.emts.alacer.helper.Logger;
import com.emts.alacer.helper.NetworkUtils;
import com.emts.alacer.helper.PreferenceHelper;
import com.emts.alacer.helper.VolleyHelper;
import com.emts.alacer.model.Notification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 4/20/2017.
 */

public class Inbox extends Fragment {
    RecyclerView mesgList;
    ArrayList<Notification> mList;
    NotificationAdapter adapter;
    ProgressBar progressBar;
    TextView errorText;
    PreferenceHelper pHelper;
    private ActionMode mActionMode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inbox, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mList = new ArrayList<>();
        pHelper = PreferenceHelper.getInstance(getContext());
        mesgList = (RecyclerView) view.findViewById(R.id.inbox_item_list);
        mesgList.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mesgList.setLayoutManager(linearLayoutManager);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.error_mesg);
        adapter = new NotificationAdapter(getActivity(), mList);

        mesgList.setAdapter(adapter);


        if (NetworkUtils.isInNetwork(getActivity())) {
            inboxListingTask();
            mesgList.setVisibility(View.GONE);
        } else {
            errorText.setText("No Internet Connected !!");
            errorText.setVisibility(View.VISIBLE);
            mesgList.setVisibility(View.GONE);
        }

        //context action mode
//        mesgList.setM
    }

    private void inboxListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParam = vHelper.getPostParams();
        postParam.put("token", pHelper.getToken());
        postParam.put("branch_id", pHelper.getString(PreferenceHelper.APP_USER_BRANCH_ID, ""));
        postParam.put("department_id", pHelper.getString(PreferenceHelper.APP_USER_DEPART_ID, ""));
//        postParam.put("department_id", "0");

        vHelper.addVolleyRequestListeners(Api.getInstance().inboxUrl, Request.Method.POST, postParam, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    Logger.printLongLog("response", response);
                    if (resObj.getBoolean("status")) {
                        JSONArray inboxArray = null;
                        try {

                            inboxArray = resObj.getJSONArray("inbox_msg");
                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                            mesgList.setVisibility(View.GONE);
                            errorText.setText(resObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                            return;
                        }
                        for (int i = 0; i < inboxArray.length(); i++) {
                            JSONObject eachObj = inboxArray.getJSONObject(i);
                            Notification notification = new Notification();
                            notification.setId(eachObj.getString("notification_id"));
                            notification.setTitle(eachObj.getString("type_name"));
                            notification.setDate(eachObj.getString("created_date"));
                            notification.setImageUrl(resObj.getString("icon_dir") + eachObj.getString("type_image"));
                            notification.setMesg(eachObj.getString("message"));
//                            notification.setAudioUrl(resObj.getString("sample_voice"));
//                            String voiceName = eachObj.getString("voice_message_saved");
                            String voiceName = eachObj.getString("voice_message");
                            if (!TextUtils.isEmpty(voiceName) && !voiceName.equalsIgnoreCase("null")) {
                                notification.setAudioUrl(resObj.getString("icon_dir") + voiceName);
                            } else {
                                notification.setAudioUrl("");
                            }
                            notification.setIncident_id(eachObj.getString("incident_id"));
                            String resId = eachObj.getString("response_id");
                            notification.setResponded(eachObj.getString("is_responded").equalsIgnoreCase("yes"));
                            if (eachObj.getString("is_responded").equalsIgnoreCase("yes")) {
                                notification.setRespondType(eachObj.getString("response"));
                            }
                            mList.add(notification);
                        }
                        adapter.notifyDataSetChanged();
                        mesgList.setVisibility(View.VISIBLE);
                    } else {
//                        AlertUtils.showSnack(getActivity(), mesgList, resObj.getString("message"));
                        mesgList.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("inboxListingTask ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    AlertUtils.showSnack(getActivity(), mesgList, errorObj.getString("message"));
                    mesgList.setVisibility(View.GONE);
                    errorText.setText(errorObj.getString("message"));
                    errorText.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    Logger.e("inboxListingTask error ex", e.getMessage() + " ");
                }
                progressBar.setVisibility(View.GONE);
            }
        }, "inboxListingTask");
    }


//
//    private class ModeCallback implements ListView.MultiChoiceModeListener {
//
//        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//            MenuInflater inflater = FoodItemListingActivity.this.getMenuInflater();
//            inflater.inflate(R.menu.list_select_menu, menu);
//            mode.setTitle(R.string.select_list_item);
//            return true;
//        }
//
//        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//            return true;
//        }
//
//        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//            switch (item.getItemId()) {
//                case R.id.delete:
//                    //get all selected item ids
////                    String selIds = "";
//                    ArrayList<Integer> checkedItemsPositions = new ArrayList<>();
//                    SparseBooleanArray selectedItemArray = mDynamicListView.getCheckedItemPositions();
//                    for (int i = 0; i < selectedItemArray.size(); i++) {
//                        int key = selectedItemArray.keyAt(i);
//                        boolean value = selectedItemArray.get(key);
//                        if (value) {
////                            checkedItemsPositions.add(mDynamicListView.getAdapter().getItemId(key));
//                            checkedItemsPositions.add(key);
//                        }
//                    }
//
//                    showDeleteAlertDialog(mode, checkedItemsPositions);
////                    mode.finish();
//
//                    break;
//                case R.id.edit:
//                    SparseBooleanArray selected = mDynamicListView.getCheckedItemPositions();
//                    int selItemPos = selected.keyAt(0);
//
//                    Intent intent = new Intent(getApplicationContext(), AddFoodItemActivity.class);
//                    intent.putExtra("food_item", list.get(selItemPos));
//                    intent.putExtra("list_position_to_update", selItemPos);
//                    intent.putExtra("cat_list", allCat);
//                    intent.putExtra("cat_id", catId);
//                    startActivityForResult(intent, REQUEST_EDIT_FOOD);
//                    mode.finish();
//
//                    break;
//                default:
//                    Toast.makeText(getApplicationContext(), "Clicked " + item.getTitle(),
//                            Toast.LENGTH_SHORT).show();
//                    break;
//            }
//            return true;
//        }
//
//        public void onDestroyActionMode(ActionMode mode) {
//        }
//
//        public void onItemCheckedStateChanged(ActionMode mode,
//                                              int position, long id, boolean checked) {
//            final int checkedCount = mDynamicListView.getCheckedItemCount();
//            if (checkedCount == 1) {
//                mode.getMenu().findItem(R.id.edit).setVisible(true);
//            } else {
//                mode.getMenu().findItem(R.id.edit).setVisible(false);
//            }
//            switch (checkedCount) {
//                case 0:
//                    mode.setSubtitle(null);
//                    break;
//                case 1:
//                    mode.setSubtitle(R.string.one_item_selected);
//                    break;
//                default:
//                    mode.setSubtitle("" + checkedCount + getString(R.string.items_selected));
//                    break;
//            }
//
//            Toast.makeText(getApplicationContext(), "-->" + position, Toast.LENGTH_SHORT).show();
//        }
//    }

    public void refreshInbox() {

        if (NetworkUtils.isInNetwork(getActivity())) {
            mesgList.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            mList.clear();
            inboxListingTask();
        } else {
            AlertUtils.showSnack(getActivity(), errorText, "Connect to Internet and refresh to update");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.e("refresh", "re");
    }


}
