package com.emts.alacer.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.emts.alacer.R;
import com.emts.alacer.activity.UserMainActivity;
import com.emts.alacer.helper.Logger;

/**
 * Created by Srijana on 4/20/2017.
 */

public class Home extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView inReporting = (ImageView) view.findViewById(R.id.img_incident_reporting);
        inReporting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UserMainActivity) getActivity()).openFragment(view.getId());
            }
        });

        ImageView inbox = (ImageView) view.findViewById(R.id.img_inbx_mascom);
        inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UserMainActivity) getActivity()).openFragment(view.getId());
            }
        });

        ImageView bcm = (ImageView) view.findViewById(R.id.img_bcm);
        bcm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UserMainActivity) getActivity()).openFragment(view.getId());
            }
        });
    }
}
